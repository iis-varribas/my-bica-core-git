/*
 * JmanagerView.java
 */
package jmanager;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import jmanager.GrDebug.GrPrimitivesAbs;
import jmanager.components.Body;
import jmanager.components.Head;
import jmanager.components.LpsViewer;
import jmanager.components.LogExport;
import javax.swing.filechooser.FileNameExtensionFilter;
import jmanager.components.Calibration;
import jmanager.components.ImageDebug;
import jmanager.components.ImageGUI;
import jmanager.field.GLFieldPanel;



/**
 * The application's main frame.
 */
public class JmanagerView extends FrameView {

	private class Component {

		private String id;
		private boolean activated = false;
		private boolean debug = false;
		private boolean _log = false;

		public void setId(String newId) {
			id = newId;
		}

		public String getId() {
			return id;
		}

		public void setActivate(boolean act) {
			activated = act;


			if (activated) {
                                //System.out.println("run "+id);
				JmanagerView.root.getConnection().schedulerPrx.run(id);
			} else {
                                //System.out.println("stop "+id);
				JmanagerView.root.getConnection().schedulerPrx.stop(id);
			}
		}

		public boolean getActivate() {
			return activated;
		}

		public void setDebug(boolean deb) {
			debug = deb;
			if (debug) {
				JmanagerView.root.getConnection().debugPrx.debugOn(id);
			} else {
				JmanagerView.root.getConnection().debugPrx.debugOff(id);
			}
		}

		public boolean getDebug() {
			return debug;
		}

		public void setLog(boolean log) {
			_log = log;
			if (_log) {
				JmanagerView.root.getConnection().logProviderPrx.logOn(id);
			} else {
				JmanagerView.root.getConnection().logProviderPrx.logOff(id);
			}
		}

		public boolean getLog() {
			return _log;
		}
	}


	private class ComponentsNameComparator implements Comparator<Component> {

		public ComponentsNameComparator() {
		}

		/**
		 * Método compare.
		 * Compara dos componentes lexicográficamente por su nombre (ID).
		 *
		 * @param compt1
		 * @param compt2
		 * @return -1, 0, ó 1 de acuerdo al método compareTo de java.lang.String.
		 */
		public int compare(Component compt1, Component compt2) {
			return compt1.getId().toLowerCase().compareTo(compt2.getId().toLowerCase());
		}
	}

	
	public static JmanagerView root;
	private boolean componentInited = false;
	private List<Component> componentsList = new ArrayList<Component>();

	/**
	 * Constructor de JmanagerView.
	 * @param app
	 */
	public JmanagerView(SingleFrameApplication app) {
		super(app);
		try {
			// Es una interfaz más agradable que la que viene por defecto.
                        // Esto es mejor cambiarlo en los resources.
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		initComponents();
		root = this;

                if (!componentInited) {
                //System.out.print("Iniciando Componentes:\n");
                componentInited = true;

                readComponentsFile();

                Iterator iter = componentsList.iterator();
                while (iter.hasNext()) {
                    Component aux = (Component) iter.next();
                    //System.out.println("" + aux.getId());
                    CompsCB.addItem(aux.getId());
                }
                System.out.println();//Separa una línea en la consola.
            }

		// status bar initialization - message timeout, idle icon and busy animation, etc
		ResourceMap resourceMap = getResourceMap();
		int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
		messageTimer = new Timer(messageTimeout, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				statusMessageLabel.setText("");
			}
		});
		messageTimer.setRepeats(false);
		int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
		for (int i = 0; i < busyIcons.length; i++) {
			busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
		}
		busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
				statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
			}
		});
		idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
		statusAnimationLabel.setIcon(idleIcon);
		progressBar.setVisible(false);

		// connecting action tasks to status bar via TaskMonitor
		TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
		taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

			public void propertyChange(java.beans.PropertyChangeEvent evt) {
				String propertyName = evt.getPropertyName();
				if ("started".equals(propertyName)) {
					if (!busyIconTimer.isRunning()) {
						statusAnimationLabel.setIcon(busyIcons[0]);
						busyIconIndex = 0;
						busyIconTimer.start();
					}
					progressBar.setVisible(true);
					progressBar.setIndeterminate(true);
				} else if ("done".equals(propertyName)) {
					busyIconTimer.stop();
					statusAnimationLabel.setIcon(idleIcon);
					progressBar.setVisible(false);
					progressBar.setValue(0);
				} else if ("message".equals(propertyName)) {
					String text = (String) (evt.getNewValue());
					statusMessageLabel.setText((text == null) ? "" : text);
					messageTimer.restart();
				} else if ("progress".equals(propertyName)) {
					int value = (Integer) (evt.getNewValue());
					progressBar.setVisible(true);
					progressBar.setIndeterminate(false);
					progressBar.setValue(value);
				}
			}
		});
	}

	@Action
	public void showAboutBox() {
		if (aboutBox == null) {
			JFrame mainFrame = JmanagerApp.getApplication().getMainFrame();
			aboutBox = new JmanagerAboutBox(mainFrame);
			aboutBox.setLocationRelativeTo(mainFrame);
		}
		JmanagerApp.getApplication().show(aboutBox);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        connectionPanel1 = new jmanager.ConnectionPanel();
        builder1 = new jmanager.VICODE.Builder();
        jPanel1 = new javax.swing.JPanel();
        CompsCB = new javax.swing.JComboBox();
        ActivateCompCB = new javax.swing.JCheckBox();
        GUIButton = new javax.swing.JButton();
        RVButton = new javax.swing.JButton();
        IVButton = new javax.swing.JButton();
        AVButton = new javax.swing.JButton();
        DebugCB = new javax.swing.JCheckBox();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setMinimumSize(new java.awt.Dimension(1024, 640));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(1024, 640));

        jTabbedPane1.setName("CompsTB"); // NOI18N
        jTabbedPane1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                jTabbedPane1ComponentShown(evt);
            }
        });

        connectionPanel1.setName("connectionPanel1"); // NOI18N
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(JmanagerView.class);
        jTabbedPane1.addTab(resourceMap.getString("connectionPanel1.TabConstraints.tabTitle"), connectionPanel1); // NOI18N

        builder1.setName("builder1"); // NOI18N
        jTabbedPane1.addTab(resourceMap.getString("builder1.TabConstraints.tabTitle"), builder1); // NOI18N

        jPanel1.setName("jPanel1"); // NOI18N

        CompsCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        CompsCB.setName("CompsCB"); // NOI18N
        CompsCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CompsCBActionPerformed(evt);
            }
        });

        ActivateCompCB.setText(resourceMap.getString("ActivateCompCB.text")); // NOI18N
        ActivateCompCB.setName("ActivateCompCB"); // NOI18N
        ActivateCompCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActivateCompCBActionPerformed(evt);
            }
        });

        GUIButton.setText(resourceMap.getString("GUIButton.text")); // NOI18N
        GUIButton.setName("GUIButton"); // NOI18N
        GUIButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GUIButtonActionPerformed(evt);
            }
        });

        RVButton.setText(resourceMap.getString("RVButton.text")); // NOI18N
        RVButton.setName("RVButton"); // NOI18N
        RVButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RVButtonActionPerformed(evt);
            }
        });

        IVButton.setText(resourceMap.getString("IVButton.text")); // NOI18N
        IVButton.setName("IVButton"); // NOI18N
        IVButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IVButtonActionPerformed(evt);
            }
        });

        AVButton.setText(resourceMap.getString("AVButton.text")); // NOI18N
        AVButton.setName("AVButton"); // NOI18N
        AVButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AVButtonActionPerformed(evt);
            }
        });

        DebugCB.setText(resourceMap.getString("DebugCB.text")); // NOI18N
        DebugCB.setName("DebugCB"); // NOI18N
        DebugCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DebugCBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(RVButton)
                    .addComponent(IVButton, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CompsCB, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AVButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DebugCB)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ActivateCompCB)
                                .addGap(18, 18, 18)
                                .addComponent(GUIButton, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(785, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CompsCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ActivateCompCB)
                    .addComponent(GUIButton))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(AVButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RVButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(IVButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DebugCB)))
                .addContainerGap(570, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(resourceMap.getString("jPanel1.TabConstraints.tabTitle"), jPanel1); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1166, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 779, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 1166, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 982, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

        private void jTabbedPane1ComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jTabbedPane1ComponentShown
            /*if (!componentInited) {
                //System.out.print("Iniciando Componentes:\n");
                componentInited = true;

                readComponentsFile();

                Iterator iter = componentsList.iterator();
                while (iter.hasNext()) {
                    Component aux = (Component) iter.next();
                    System.out.println("" + aux.getId());
                    CompsCB.addItem(aux.getId());
                }
                System.out.println();//Separa una línea en la consola.
            }*/
}//GEN-LAST:event_jTabbedPane1ComponentShown

        private void RVButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RVButtonActionPerformed
            String title = ((javax.swing.JButton)evt.getSource()).getText();
            javax.swing.JFrame compFrame = new javax.swing.JFrame(title);
            LpsViewer lpsv = new LpsViewer();
            lpsv.setFrame(compFrame);
            compFrame.add(lpsv);
            compFrame.setSize(1110, 450);
            compFrame.setVisible(true);
            compFrame.addWindowListener(lpsv);
        }//GEN-LAST:event_RVButtonActionPerformed

        private void GUIButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GUIButtonActionPerformed
            Component aux = getSelectedCL();

            if (aux.getId().equals(new String("Body"))) {
                javax.swing.JFrame compFrame = new javax.swing.JFrame("Body");
                Body body = new Body();
                compFrame.add(body);
                compFrame.setSize(800, 600);
                compFrame.setVisible(true);
            }

            if (aux.getId().equals(new String("Head"))) {
                javax.swing.JFrame compFrame = new javax.swing.JFrame("Head");
                Head head = new Head();
                compFrame.add(head);
                compFrame.setSize(250, 400);
                compFrame.setVisible(true);
            }
            
            if (aux.getId().equals(new String("Calibration"))) {
                javax.swing.JFrame compFrame = new javax.swing.JFrame("Calibration");
                Calibration calibration = new Calibration();
                compFrame.add(calibration);
                compFrame.setSize(250, 400);
                compFrame.setVisible(true);
            }
}//GEN-LAST:event_GUIButtonActionPerformed

        private void ActivateCompCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActivateCompCBActionPerformed
            Component aux = getSelectedCL();
            aux.setActivate(ActivateCompCB.isSelected());
            aux.setDebug(DebugCB.isSelected());
        }//GEN-LAST:event_ActivateCompCBActionPerformed

        private void CompsCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CompsCBActionPerformed

       
            Component aux = getSelectedCL();

            ActivateCompCB.setSelected(aux.getActivate());
            DebugCB.setSelected(aux.getDebug());
        }//GEN-LAST:event_CompsCBActionPerformed

        private void AVButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AVButtonActionPerformed
        String title = ((javax.swing.JButton)evt.getSource()).getText();
        javax.swing.JFrame compFrame = new javax.swing.JFrame(title);

        GrPrimitivesAbs grPrimitivesAbs = new GrPrimitivesAbs();
        GLFieldPanel fldv = new GLFieldPanel(grPrimitivesAbs);
        compFrame.add(fldv);
        compFrame.setSize(fldv.getPreferredSize());
        compFrame.setVisible(true);
        compFrame.addWindowListener(fldv);        
        }//GEN-LAST:event_AVButtonActionPerformed

        private void IVButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IVButtonActionPerformed
            String title = ((javax.swing.JButton)evt.getSource()).getText();
            javax.swing.JFrame compFrame = new javax.swing.JFrame(title);
            ImageDebug imagegui = new ImageDebug();
            imagegui.setFrame(compFrame);
            compFrame.add(imagegui);
            compFrame.setSize(1110, 450);
            compFrame.setVisible(true);
            compFrame.addWindowListener(imagegui);
        }//GEN-LAST:event_IVButtonActionPerformed

        private void DebugCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DebugCBActionPerformed
		Component aux = getSelectedCL();
		aux.setDebug(DebugCB.isSelected());
 
        }//GEN-LAST:event_DebugCBActionPerformed

	private void readComponentsFile() {
		File file = new File("conf/components.conf");

		try {
			BufferedReader bis = new BufferedReader(new FileReader(file));
			String line = null;

			while ((line = bis.readLine()) != null) {
				Component auxcl = new Component();
				auxcl.setId(line);
				componentsList.add(auxcl);
			}

			bis.close();

			//Ordena la lista de componentes por nombre.
			Collections.sort(componentsList, new ComponentsNameComparator());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Component getSelectedCL() {
		Component ret = null;
		String selected = null;

		if (CompsCB.getItemCount() > 0) {
			selected = (String) CompsCB.getItemAt(CompsCB.getSelectedIndex());
		}

                //System.out.println(selected);

		if (selected != null) {

			Iterator iter = componentsList.iterator();
			while (iter.hasNext()) {
				Component aux = (Component) iter.next();
				if (aux.getId().equals(selected)) {
					ret = aux;
				}
			}
		}
		return ret;
	}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AVButton;
    private javax.swing.JCheckBox ActivateCompCB;
    private javax.swing.JComboBox CompsCB;
    private javax.swing.JCheckBox DebugCB;
    private javax.swing.JButton GUIButton;
    private javax.swing.JButton IVButton;
    private javax.swing.JButton RVButton;
    private jmanager.VICODE.Builder builder1;
    private jmanager.ConnectionPanel connectionPanel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables
	private final Timer messageTimer;
	private final Timer busyIconTimer;
	private final Icon idleIcon;
	private final Icon[] busyIcons = new Icon[15];
	private int busyIconIndex = 0;
	private JDialog aboutBox;

	public Connection getConnection() {
		return connectionPanel1.getConnection();
	}

	public ConnectionPanel getConnectionPanel() {
		return connectionPanel1;
	}


}
