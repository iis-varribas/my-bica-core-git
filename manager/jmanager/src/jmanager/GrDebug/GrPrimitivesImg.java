/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jmanager.GrDebug;

import bica.*;
import java.util.Observable;

/**
 *
 * @author carlos
 */
public class GrPrimitivesImg extends Observable{

    private bica.Shape[] shapeList;

    public GrPrimitivesImg() {
       shapeList =  null;
    }

    public void setGrPrimitives(bica.Shape[] newShapeList){
        this.shapeList = newShapeList;
        this.setChanged();
	this.notifyObservers();
    }

    public Shape[] getGrPrimitives(){
        return this.shapeList;
    }
}
