/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jmanager.components;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class ImageThread extends Thread{

	ImageDebug imageGUI = null;
	boolean stop = false;

	ImageThread(ImageDebug mypp) {
		imageGUI = mypp;
		stop = false;
	}

	@Override
	public void run() {
		stop = false;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ImageThread.class.getName()).log(Level.SEVERE, null, ex);
        }

		while (!stop) {
			imageGUI.refreshImage();
			try {
				Thread.sleep(500); // 2 Hz.
			} catch (InterruptedException ex) {
				Logger.getLogger(ImageThread.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public void stopThread() {
		stop = true;
	}
}
