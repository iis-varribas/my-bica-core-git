#ifndef ComponentLoader_H
#define ComponentLoader_H

#include "Common.h"


#include "BumperDetector.h"
#include "LFootLongPress.h"
#include "RFootLongPress.h"
#include "EvitaObstaculos.h"
#include "SitDownStandUp.h"
#include "MyBallDetector.h"
#include "NewBallDetector.h"
#include "ColorConfigure.h"
#include "SiguePelota2d.h"
#include "SiguePelota3d.h"
#include "GoToBall.h"
#include "BallSearch.h"
#include "KickBall.h"
#include "GoalDetector.h"


class ComponentLoader
{

public:
	static void initComponents(AL::ALPtr<AL::ALBroker> parentBroker){
		BumperDetector::getInstance()->init("BumperDetector", parentBroker);
		LFootLongPress::getInstance()->init("LFootLongPress", parentBroker);
		RFootLongPress::getInstance()->init("RFootLongPress", parentBroker);

		EvitaObstaculos::getInstance()->init("EvitaObstaculos", parentBroker);
		SitDownStandUp::getInstance()->init("SitDownStandUp", parentBroker);

		MyBallDetector::getInstance()->init("MyBallDetector", parentBroker);
		NewBallDetector::getInstance()->init("NewBallDetector", parentBroker);
		GoalDetector::getInstance()->init("GoalDetector", parentBroker);
		ColorConfigure::getInstance()->init("ColorConfigure", parentBroker);
                
                SiguePelota2d::getInstance()->init("SiguePelota2d", parentBroker);
                SiguePelota3d::getInstance()->init("SiguePelota3d", parentBroker);
                GoToBall::getInstance()->init("GoToBall", parentBroker);
                
                BallSearch::getInstance()->init("BallSearch", parentBroker);
		KickBall::getInstance()->init("KickBall", parentBroker);


	}
};

#endif
		
