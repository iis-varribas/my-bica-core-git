
#ifndef IMAGE_ICE
#define IMAGE_ICE

#include "common.ice"

module bica {

  enum FeaturesType { Detected, Filtered, Ball, BlueNet, YellowNet, RedMarker, BlueMarker, Field, Lines, Regions, Segments, HSV };
  enum ObjectsType { BallObj, BlueNetObj, YellowNetObj, RedMarkerObj, BlueMarkerObj, FieldObj, LinesObj };
  enum CameraType { UPPERCAMERA, LOWERCAMERA };
    
  /**
   *  Static description of the image source.
   */
  class ImageDescription 
  {
    int width; /**< %Image width [pixels] */
    int height;/**< %Image height [pixels] */
    int size;/**< %Image size [bytes] */
    string format; /**< %Image format string */
  };
  
  /**
  * A single image served as a sequence of bytes
  */
  class ImageData
  { 
    Time timeStamp; /**< TimeStamp of Data */
    ImageDescription description; /**< ImageDescription of Data, for convienence purposes */
    ByteSeq pixelData; /**< The image data itself. The structure of this byte sequence
			  depends on the image format and compression. */
  };
  
  class HSVFilter
  {
  	float hmin;
  	float hmax;
  	float smin;
  	float smax;
  	float vmin;
  	float vmax;
  };
  
    
  /** 
  * Interface to the image provider.
  */
  interface ImageProvider
  {
    /** 
     * Returns the image source description.
     */
    idempotent ImageDescription getImageDescription();

    /**
     * Returns the latest data.
     */
    idempotent ImageData getImageData();

    string test();    
  };
    
};

#endif //IMAGE_ICE
