#ifndef COMPONENTSI_ICE
#define COMPONENTSI_ICE

#include "common.ice"
#include "image.ice"

module bica {
 
  interface WorldModelProvider
  {
    /** 
     * Returns the current world model.
     */
    idempotent string getWorldModel();    
  };
  
  /** 
  * Interface to the GHead component.
  */
  interface GHeadManager
  {
  	idempotent void lookAt( float x, float y, float z);
  };
  
 
  /** 
  * Interface to the Obstacles component.
  */
  interface ObstaclesManager
  {
  	idempotent ImageData getDbgImg();
  };
  
  /** 
  * Interface to a generic component.
  */
  interface ComponentInfoProvider
  {
  	idempotent string getDebugData(string component);
  };
  
  
  
   /**
   *  Static description of the calibration params
   */
  class CalibrationParams
  {
  	float robotx;
	float roboty;
	float robott;
	float u0;
	float v0;
	float fdistx;
	float fdisty;
	float roll;
  };
  /** 
  * Interface to the calibration.
  */
  interface CalibrationProvider
  {
    /** 
     * 
     */
    idempotent CalibrationParams getCalibrationParams();
    idempotent void setCalibrationParams (CalibrationParams newParams);
    idempotent void saveCalibrationParams ();
    idempotent void setCameraParams ();
    
    };
  
  
  /**
   *  Static description of the visual memory object.
   */
  class VisualMemoryObj 
  {
    float x;
    float y;
   	float dx;
    float dy;
    float quality;
    float time;
    string reliability;
  };
  /**
  * Interface to the ball detector
  */
  interface BallDetectorManager
  {
	idempotent VisualMemoryObj getVisualMemoryObject();
	idempotent void predictionUpdate();
	idempotent void measurementUpdate();
	
  };
 

   /**
  * Interface to the goal detector
  */
  interface GoalDetectorManager
  {
  	idempotent VisualMemoryObj getVisualMemoryObject(string obj);
	idempotent void predictionUpdate();
	idempotent void measurementUpdate();
  };
    
};

#endif //COMPONENTSI_ICE
