#include "KickBall.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

KickBall::KickBall()
{
	_BallSearch = BallSearch::getInstance();
	_NewBallDetector = NewBallDetector::getInstance();
	_GoToBall = GoToBall::getInstance();
	_SiguePelota3d = SiguePelota3d::getInstance();
	_Body = Body::getInstance();
	state = Initial;
}

KickBall::~KickBall()
{

}

void KickBall::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void KickBall::SearchingBall_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SearchingBall_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"KickBall [SearchingBall]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end SearchingBall_state_code
}

void KickBall::GoToBall_State_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin GoToBall_State_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"KickBall [GoToBall]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end GoToBall_State_state_code
}

void KickBall::Patear_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Patear_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"KickBall [Patear]\n";
}

	_Body->selKick(Body::TXTFWNR);
//BUILDER COMMENT. DO NOT REMOVE. end Patear_state_code
}

void KickBall::Patenado_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Patenado_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"KickBall [Pateando]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end Patenado_state_code
}

bool KickBall::Initial2SearchingBall0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2SearchingBall0_transition_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial2SearchingBall0_transition_code
}

bool KickBall::SearchingBall2GoToBall_State0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SearchingBall2GoToBall_State0_transition_code
	return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end SearchingBall2GoToBall_State0_transition_code
}

bool KickBall::GoToBall_State2SearchingBall0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin GoToBall_State2SearchingBall0_transition_code
	return !_NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end GoToBall_State2SearchingBall0_transition_code
}

bool KickBall::Patear2GoToBall_State0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Patear2GoToBall_State0_transition_code
//	return _NewBallDetector->isDetected();
//	_Body->selKick(Body::TXTFWNR);
//	return true;
	return false;
//BUILDER COMMENT. DO NOT REMOVE. end Patear2GoToBall_State0_transition_code
}

bool KickBall::GoToBall_State2Patear0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin GoToBall_State2Patear0_transition_code
	return _GoToBall->moveIsDone();
//BUILDER COMMENT. DO NOT REMOVE. end GoToBall_State2Patear0_transition_code
}

bool KickBall::Patear2Patenado0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Patear2Patenado0_transition_code
	return true;
//BUILDER COMMENT. DO NOT REMOVE. end Patear2Patenado0_transition_code
}

bool KickBall::Patenado2SearchingBall0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Patenado2SearchingBall0_transition_code
	return _Body->isStopped();
//BUILDER COMMENT. DO NOT REMOVE. end Patenado2SearchingBall0_transition_code
}

void
KickBall::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2SearchingBall0_transition_code()) {
				state = SearchingBall;
			}
		}

		break;
	case SearchingBall:
		_BallSearch->step();
		_NewBallDetector->step();

		if (isTime2Run()) {
			SearchingBall_state_code();

			if (SearchingBall2GoToBall_State0_transition_code()) {
				state = GoToBall_State;
			}
		}

		break;
	case GoToBall_State:
		_GoToBall->step();
		_SiguePelota3d->step();
		_NewBallDetector->step();

		if (isTime2Run()) {
			GoToBall_State_state_code();

			if (GoToBall_State2SearchingBall0_transition_code()) {
				state = SearchingBall;
			}
			else if (GoToBall_State2Patear0_transition_code()) {
				state = Patear;
			}
		}

		break;
	case Patear:
		_NewBallDetector->step();

		if (isTime2Run()) {
			Patear_state_code();

			if (Patear2GoToBall_State0_transition_code()) {
				state = GoToBall_State;
			}
			else if (Patear2Patenado0_transition_code()) {
				state = Patenado;
			}
		}

		_Body->step();
		break;
	case Patenado:
		_Body->step();

		if (isTime2Run()) {
			Patenado_state_code();

			if (Patenado2SearchingBall0_transition_code()) {
				state = SearchingBall;
			}
		}

		break;
	default:
		cout << "[KickBall::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

