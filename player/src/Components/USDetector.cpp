/*
 * Name: USDetector.cpp
 * @Author: Alejandro Aporta (a.aporta@alumnos.urjc.es)
 * @Author: Francisco Martín (fmartin@gsyc.es)
 
 * 
 * Description: Class to detect other robots
 *
 * Created on: 04/11/2011
 *
 * Copyright (C) Universidad Rey Juan Carlos
 * All Rights Reserved.
 *
 */

#include "USDetector.h"
 

USDetector::USDetector()
{
	body = Body::getInstance();

}

USDetector::~USDetector()
{
}

void
USDetector::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker) {
	Component::init(newName, parentBroker);
	

	try
	{
		pmemory = getParentBroker()->getMemoryProxy();
	}catch( AL::ALError& e )
	{
		cerr<<"USDetector::init [pmemory]"<<e.toString()<<endl;
	}

	try
	{
		pmotion = getParentBroker()->getMotionProxy();
	}catch( AL::ALError& e )
	{
		cerr<<"USDetector::init [pmotion]"<<e.toString()<<endl;
	}
	
	setFreqTime(MEDIUM_RATE);
}

void
USDetector::dowork(void)
{
	MatrixCM RTdiff(4,4),RTUS1(4,4),us1rel(4,1);
	MatrixCM RTTorso(4,4);
	MatrixCM us1Robot(4,1), us2Robot(4,1);
	
	double degX =0.0;
	double degY =-0.17450;
	double degZ =-0.34900;

	double despX =53.7;
	double despY =34.1;
	double despZ =69.8;	
	
	torsoRT = this->pmotion->getTransform("Torso", 2, true);
	
	RTTorso.sete(0,0,torsoRT[0]);
	RTTorso.sete(0,1,torsoRT[1]);
	RTTorso.sete(0,2,torsoRT[2]);
	RTTorso.sete(0,3,torsoRT[3]);
	RTTorso.sete(1,0,torsoRT[4]);
	RTTorso.sete(1,1,torsoRT[5]);
	RTTorso.sete(1,2,torsoRT[6]);
	RTTorso.sete(1,3,torsoRT[7]);
	RTTorso.sete(2,0,torsoRT[8]);
	RTTorso.sete(2,1,torsoRT[9]);
	RTTorso.sete(2,2,torsoRT[10]);
	RTTorso.sete(2,3,torsoRT[11]);
	RTTorso.sete(3,0,torsoRT[12]);
	RTTorso.sete(3,1,torsoRT[13]);
	RTTorso.sete(3,2,torsoRT[14]);
	RTTorso.sete(3,3,torsoRT[15]);

	RTdiff.sete(0,0,cos(degY)*cos(degZ));
	RTdiff.sete(0,1,-cos(degX)*sin(degZ));
	RTdiff.sete(0,2,cos(degX)*sin(degY)*cos(degZ));
	RTdiff.sete(0,3,despX);                         /*Profundidad*/
	RTdiff.sete(1,0,cos(degY)*sin(degZ));
	RTdiff.sete(1,1,cos(degX)*cos(degZ));
	RTdiff.sete(1,2,cos(degX)*sin(degY)*cos(degZ));
	RTdiff.sete(1,3,despY);                                 /*Lateral*/
	RTdiff.sete(2,0,-sin(degY));
	RTdiff.sete(2,1,0.0);
	RTdiff.sete(2,2,cos(degX)*cos(degY));
	RTdiff.sete(2,3,despZ);                         /*Altura*/
	RTdiff.sete(3,0,0.0);
	RTdiff.sete(3,1,0.0);
	RTdiff.sete(3,2,0.0);
	RTdiff.sete(3,3,1.0);
	
	RTUS1 = RTTorso * RTdiff;
	
	/*Medida del sensor en relativas al sensor*/
	us1rel.sete(0,0, sonar_value_right * 1000.0);
	us1rel.sete(1,0,0.0);
	us1rel.sete(2,0,0.0);
	us1rel.sete(3,0,1.0);
	
	/*Sensor 1 en relativas al robot*/
	us1Robot = RTUS1 * us1rel;


	degZ =0.34900;

	RTdiff.sete(0,0,cos(degY)*cos(degZ));
	RTdiff.sete(0,1,-cos(degX)*sin(degZ));
	RTdiff.sete(0,2,cos(degX)*sin(degY)*cos(degZ));
	RTdiff.sete(1,0,cos(degY)*sin(degZ));
	RTdiff.sete(1,1,cos(degX)*cos(degZ));
	RTdiff.sete(1,2,cos(degX)*sin(degY)*cos(degZ));
	RTdiff.sete(2,3,despZ);                         /*Altura*/
	us1rel.sete(0,0, sonar_value_left*1000.0);

	RTUS1 = RTTorso * RTdiff;
	us2Robot = RTUS1 * us1rel;

	left.X = us2Robot.e(0,0)/us2Robot.e(3,0);
	left.Y = us2Robot.e(1,0)/us2Robot.e(3,0);
	left.Z = us2Robot.e(2,0)/us2Robot.e(3,0);
	left.H = us2Robot.e(3,0)/us2Robot.e(3,0);
	right.X = us1Robot.e(0,0)/us1Robot.e(3,0);
	right.Y = us1Robot.e(1,0)/us1Robot.e(3,0);
	right.Z = us1Robot.e(2,0)/us1Robot.e(3,0);
	right.H = us1Robot.e(3,0)/us1Robot.e(3,0);
}

void
USDetector::getValues(HPoint3D &rleft, HPoint3D &rright)
{
	rleft = left;
	rright = right;
}

void
USDetector::step()
{
	
	if (!isTime2Run())
		return;

	try{
		
		sonar_value_right = (double)(pmemory->getData(string("Device/SubDeviceList/US/Right/Sensor/Value"), 1));
		sonar_value_left = (double)(pmemory->getData(string("Device/SubDeviceList/US/Left/Sensor/Value"), 1));

		dowork();
		

	} catch (ALError& e)
	{
		cerr << "USDetector::step(ALMemory->getData)" << e.toString() << endl;
	}
	
}


bica::ShapeList
USDetector::getGrDebugRel()
{
	shapeListRel.clear();

	bica::Point3DPtr p1(new bica::Point3D);
	bica::Point3DPtr p2(new bica::Point3D);
	bica::ArrowPtr arrow(new bica::Arrow);

	p1->x = 0.0f;
	p1->y = 0.0f;
	p1->z = 0.0f;

	p2->x = left.X;
	p2->y = left.Y;
	p2->z = left.Z;

	arrow->src = p1;
	arrow->dst = p2;
	arrow->width = 2;
	arrow->color = bica::ORANGE;
	arrow->filled = false;
	arrow->opacity = 254;
	arrow->accKey = "b";
	arrow->label = "";
	shapeListRel.push_back(arrow);

	bica::Point3DPtr p3(new bica::Point3D);
	bica::Point3DPtr p4(new bica::Point3D);
	bica::ArrowPtr arrow2(new bica::Arrow);

	p3->x = 0.0f;
	p3->y = 0.0f;
	p3->z = 0.0f;

	p4->x = right.X;
	p4->y = right.Y;
	p4->z = right.Z;

	arrow2->src = p3;
	arrow2->dst = p4;
	arrow2->width = 2;
	arrow2->color = bica::ORANGE;
	arrow2->filled = false;
	arrow2->opacity = 254;
	arrow2->accKey = "b";
	arrow2->label = "";
	shapeListRel.push_back(arrow2);


	return shapeListRel;
}

