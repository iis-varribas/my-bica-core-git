#include "USDMean.h"

USDMean::USDMean(){
    	_USDetector = USDetector::getInstance();
        
	pos = 0;
        this->setFreqTime(_USDetector->getFreqTime());
}

USDMean::~USDMean(){

}

void
USDMean::getValues(HPoint3D &rleft, HPoint3D &rright){
	rleft  = left;
	rright = right;
}

void
USDMean::loadValues(){
	_USDetector->getValues(v_left[pos], v_right[pos]);
	pos = (pos+1) % SIZE;
}

void
USDMean::calc(int N){
	int i;
	if (N > 0) {
		i = 0;
		left.X = v_left[i].X;
		left.Y = v_left[i].Y;
		left.Z = v_left[i].Z;

		right.X = v_right[i].X;
		right.Y = v_right[i].Y;
		right.Z = v_right[i].Z;
	}

	for (i=1; i<N; i++) {
		left.X += v_left[i].X;
		left.Y += v_left[i].Y;
		left.Z += v_left[i].Z;

		right.X += v_right[i].X;
		right.Y += v_right[i].Y;
		right.Z += v_right[i].Z;
	}
	if (N > 0) {
		left.X /= N;
		left.Y /= N;
		left.Z /= N;

		right.X /= N;
		right.Y /= N;
		right.Z /= N;
	}
#ifdef COMPONENT_DEBUG
	cout<<"USDMean [R:"<<right.X<<","<<right.Y<<","<<right.Z<<", L:"<<left.X<<","<<left.Y<<","<<left.Z<<"]\n";
#endif
}

void
USDMean::step(){
	
switch(state){
	case 0:
		_USDetector->step();
		if (isTime2Run()) {
			loadValues();
			calc(pos);
			if (pos == 0) state = 1;
		}
	break;
	case 1:
		_USDetector->step();
		if (isTime2Run()) {
			loadValues();
			calc(SIZE);
		}
	break;
	}
}

