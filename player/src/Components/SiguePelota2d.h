#ifndef SiguePelota2d_H
#define SiguePelota2d_H

#include "Component.h"
#include "Body.h"
#include "NewBallDetector.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "head/head/Head.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class SiguePelota2d : public Component, public Singleton<SiguePelota2d>
{
public:

	SiguePelota2d();
	~SiguePelota2d();

	void step();
private:

	static const int Initial	= 0;
	static const int MueveCabeza	= 1;
	static const int BuscaPelota	= 2;
	int state;

	Head *_Head;
	Body *_Body;
	NewBallDetector *_NewBallDetector;

	void Initial_state_code(void);
	void MueveCabeza_state_code(void);
	void BuscaPelota_state_code(void);
	bool Initial2BuscaPelota0_transition_code(void);
	bool MueveCabeza2BuscaPelota0_transition_code(void);
	bool BuscaPelota2MueveCabeza0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
        public:
            static const float movement_threshold = 0.0;
private:
        HPoint2D ball_p2d;
        
	double centroX;
	double difX;
	double distX;
	double centroY;
	double difY;
	double distY;

	double tFueraX;
	double tFueraY;

	double diffXAnt;
	double diffYAnt;
	double derivX;
	double derivY;

//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

