#include "ColorConfigure.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

ColorConfigure::ColorConfigure()
{
	_CameraRGB = CameraRGB::getInstance();
	_BumperDetector = BumperDetector::getInstance();
	_LFootLongPress = LFootLongPress::getInstance();
	_NewBallDetector = NewBallDetector::getInstance();
	state = Initial;
        
        this->setFreqTime(_CameraRGB->getFreqTime());
}

ColorConfigure::~ColorConfigure()
{

}

void ColorConfigure::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//        setFreqTime(ImageInput::CAPTURE_RATE);
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void ColorConfigure::SetColor_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SetColor_state_code
	long long H, S, V, N;
	double varianza_h, varianza_s, varianza_v;
	double desviacion_h, desviacion_s, desviacion_v;
	image::hsv_color hsv_mean;

	H=S=V=N=0;
	varianza_h = varianza_s = varianza_v = 0;

	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);


	for (int h=H_m; h<H_M; h++){
		for (int w=W_m; w<W_M; w++){
			image::hsv_color hsv;
			image::rgb2hsv(imagenRGB.pixel[h][w], hsv);

			H += hsv.h;
			S += hsv.s;
			V += hsv.v;
			N++;

			varianza_h += (hsv.h*hsv.h);
			varianza_s += (hsv.s*hsv.s);
			varianza_v += (hsv.v*hsv.v);
		}
	}

	hsv_mean.h = H/N;
	hsv_mean.s = S/N;
	hsv_mean.v = V/N;

	varianza_h /= N;
	varianza_s /= N;
	varianza_v /= N;

	varianza_h -= hsv_mean.h*hsv_mean.h;
	varianza_s -= hsv_mean.s*hsv_mean.s;
	varianza_v -= hsv_mean.v*hsv_mean.v;

	desviacion_h = sqrt(varianza_h);
	desviacion_s = sqrt(varianza_s);
	desviacion_v = sqrt(varianza_v);


	hsv_min.h = (desviacion_h > hsv_mean.h)? 0 : hsv_mean.h - desviacion_h;
	hsv_min.s = (desviacion_s > hsv_mean.s)? 0 : hsv_mean.s - desviacion_s;
	hsv_min.v = (desviacion_v > hsv_mean.v)? 0 : hsv_mean.v - desviacion_v;

	hsv_max.h = (desviacion_h > 255-hsv_mean.h)? 255 : hsv_mean.h + desviacion_h;
	hsv_max.s = (desviacion_s > 255-hsv_mean.s)? 255 : hsv_mean.s + desviacion_s;
	hsv_max.v = (desviacion_v > 255-hsv_mean.v)? 255 : hsv_mean.v + desviacion_v;
        
        printf("VALORES DAVID\n");
        printf("hsv_min.h %d\n",hsv_min.h);
        printf("hsv_min.s %d\n",hsv_min.s);
        printf("hsv_min.v %d\n",hsv_min.v);
        printf("hsv_max.h %d\n",hsv_max.h);
        printf("hsv_max.s %d\n",hsv_max.s);
        printf("hsv_max.v %d\n\n",hsv_max.v);
        
//BUILDER COMMENT. DO NOT REMOVE. end SetColor_state_code
}

void ColorConfigure::Umbralized_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Umbralized_state_code
	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);
	image::rgb_color positive = {255,255,255};
	image::rgb_color negative = {0,0,0};
	image::filter::umbralizar(&imagenRGB, funcionTest, positive, negative);

        bool test_value;
#ifdef WEBOTS
        test_value = getStopWatch() > 10*1000;
#else
	test_value = _LFootLongPress->isActived();
#endif
        
	if ( test_value ){
		_NewBallDetector->setBallColors(hsv_min, hsv_max);
                cout<<"New color values setted"<<endl;
	}
//BUILDER COMMENT. DO NOT REMOVE. end Umbralized_state_code
}

void ColorConfigure::Unused_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Unused_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Unused_state_code
}

bool ColorConfigure::Initial2SetColor0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2SetColor0_transition_code
	return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2SetColor0_transition_code
}

bool ColorConfigure::SetColor2Umbralized_LFoot_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SetColor2Umbralized_LFoot_transition_code
	bool test_value;

#ifdef WEBOTS
        test_value = getStopWatch() > 10*1000;
#else
	test_value = _BumperDetector->isLeftFootPressed();
#endif

	if (test_value){
		printf("[ColorConfigure] HSV:\n min=[%d %d %d]\n max=[%d,%d,%d]\n",

			hsv_min.h, hsv_min.s, hsv_min.v,
			hsv_max.h, hsv_max.s, hsv_max.v
		);

		return true;
	}else{
		return false;
	}
//BUILDER COMMENT. DO NOT REMOVE. end SetColor2Umbralized_LFoot_transition_code
}

bool ColorConfigure::Umbralized2SetColor_RFoot_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Umbralized2SetColor_RFoot_transition_code
	bool test_value;
#ifdef WEBOTS
        test_value = getStopWatch() > 10*1000;
#else
	test_value = _BumperDetector->isRightFootPressed();
#endif

	return test_value;
//BUILDER COMMENT. DO NOT REMOVE. end Umbralized2SetColor_RFoot_transition_code
}

void
ColorConfigure::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2SetColor0_transition_code()) {
				state = SetColor;
				resetStopWatch();
			}
		}

		break;
	case SetColor:
		_CameraRGB->step();
		_BumperDetector->step();

		if (isTime2Run()) {
			SetColor_state_code();

			if (SetColor2Umbralized_LFoot_transition_code()) {
				state = Umbralized;
				resetStopWatch();
			}
		}

		break;
	case Umbralized:
		_CameraRGB->step();
		_LFootLongPress->step();
		_BumperDetector->step();

		if (isTime2Run()) {
			Umbralized_state_code();

			if (Umbralized2SetColor_RFoot_transition_code()) {
				state = SetColor;
				resetStopWatch();
			}
		}

		break;
	case Unused:

		if (isTime2Run()) {
			Unused_state_code();

		}

		_NewBallDetector->step();
		break;
	default:
		cout << "[ColorConfigure::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin

image::hsv_color ColorConfigure::hsv_min;
image::hsv_color ColorConfigure::hsv_max;

bool
ColorConfigure::funcionTest(const image::rgb_color *pixel){
	image::hsv_color hsv;
	image::rgb2hsv(*pixel, hsv);
	if (	   (hsv.h >= hsv_min.h && hsv.h <= hsv_max.h)
		&& (hsv.s >= hsv_min.s && hsv.s <= hsv_max.s)
		&& (hsv.v >= hsv_min.v && hsv.v <= hsv_max.v)
	){
		return true;
	}else{
		return false;
	}
}


bica::ShapeList 
ColorConfigure::getGrDebugImg()
{
	shapeListImg.clear();

	bica::ImagePtr image(new bica::Image);
	image->pixelData.resize(imagenRGB.SIZE);
	image->width = imagenRGB.WIDTH;
	image->height = imagenRGB.HEIGHT;
	image->color = bica::RED;
	image->filled = false;
	image->opacity = 125;
	image->accKey = "b";
	image->label = "";
	memmove( &(image->pixelData[0]), (char *) imagenRGB.pixel, imagenRGB.SIZE);
	shapeListImg.push_back(image);

	if (state == SetColor) {
		bica::Point3DPtr p1(new bica::Point3D);
		bica::Point3DPtr p2(new bica::Point3D);
		bica::RectanglePtr r(new bica::Rectangle);

		p1->x = W_m;
		p1->y = H_m;
		p1->z = 0;
		p2->x = W_M;
		p2->y = H_M;
		p2->z = 0.0;

		r->p1 = p1;
		r->p2 = p2;
		r->color = bica::BLACK;
		r->filled = false;
		r->opacity = 255;
		r->accKey = "b";
		r->label = "";

		shapeListImg.push_back(r);
	}

	return shapeListImg;
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

