#ifndef KickBall_H
#define KickBall_H

#include "Component.h"
#include "BallSearch.h"
#include "NewBallDetector.h"
#include "GoToBall.h"
#include "SiguePelota3d.h"
#include "Body.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class KickBall : public Component, public Singleton<KickBall>
{
public:

	KickBall();
	~KickBall();

	void step();
private:

	static const int Initial	= 0;
	static const int SearchingBall	= 1;
	static const int GoToBall_State	= 2;
	static const int Patear	= 3;
	static const int Patenado	= 4;
	int state;

	BallSearch *_BallSearch;
	NewBallDetector *_NewBallDetector;
	GoToBall *_GoToBall;
	SiguePelota3d *_SiguePelota3d;
	Body *_Body;

	void Initial_state_code(void);
	void SearchingBall_state_code(void);
	void GoToBall_State_state_code(void);
	void Patear_state_code(void);
	void Patenado_state_code(void);
	bool Initial2SearchingBall0_transition_code(void);
	bool SearchingBall2GoToBall_State0_transition_code(void);
	bool GoToBall_State2SearchingBall0_transition_code(void);
	bool Patear2GoToBall_State0_transition_code(void);
	bool Patenado2SearchingBall0_transition_code(void);
	bool GoToBall_State2Patear0_transition_code(void);
	bool Patear2Patenado0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
private:
	int debug_state;
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

