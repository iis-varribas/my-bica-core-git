/* 
 * Name: algoritmoII.h
 * @Author: Victor Arribas (v.arribas.urjc@gmail.com)
 * Copyright (c) 2012
 *
 * Basado en la pr�ctica de C/C++ de Seguimiento de Objetos realizada en 2008.
 * Antecedentes:
 *  - Reconocimiento de objetos por sustraci�n del fondo
 *  - Se lanzan particulas sobre la imagen y se seleccionan las mejores por peso
 *  - Cada part�cula tiene un �rea (rectangular) de influencia.
 *  - El peso de una part�cula es el n� de pixeles detectados dentro del area de influencia. 
 *  - Las siguientes poblaciones de part�culas aparecen en el entorno de las mejores
 *  - Tama�o del area de influencia din�mico (creciente y decreciente)
 * 
 * Precedentes
 *  - La memoria din�mica en BICA es un gran enemigo. (Memory leak por interrupcion de la ejecuci�n).
 *  - El proceso debe ser extremadamente r�pido
 * 
 * APROX 1:
 *  Posicionamiento de la poblaci�n de part�culas basada en una malla est�tica
 *  Se mantiene �nicamente la mejor part�cula
 *    pro: solo se necesita almacenar 2 part�culas (la mejor y la pretendiente)
 * 
 * APROX 2:
 *  aprox 1 + crecimiento del �rea de influencia
 *   pro: la part�cula ya recuadra la imagen
 *   contra: computaci�n elevada cuando los tama�os son muy diferentes
 *   contra: un recuadro inicial grande es terrible
 * 
 * APROX 3:
 *  aprox 2 con crecimiento de todas las part�culas de la poblaci�n
 *   pro: la selecci�n de part�culas es siempre a mejor. El tama�o del recuadro es acumulativo
 *   contra: computacionalmente inviable
 * 
 * APROX 4:
 *  aprox 2 + decrecimiento del area de influencia
 *   pro: el recuadro se adapta como un guante
 *   contra: el tiempo...
 * 
 * APROX 5:
 *  c�lculo del centro del objeto a partir de muestras concretas (part�culas)
 *   - El centro del objeto es el punto medio (media) de las muestras positivas
 *   - Las muestras son part�culas de tama�o 1 (peso={1,0}) diseminadas de forma uniforme (malla de nudos est�tica)
 *   - A partir de las part�culas m�s alejadas se puede extraer de forma aproximada los l�mites del objeto
 *   pro: no se calculan pesos
 * 
 * APROX 6:
 *  aprox 5 + resoluci�n de malla din�mica
 *   - La resoluci�n de la malla es dependiente del tama�o del objeto.
 *   pro: minimiza el n� de particulas
 *   contra: resultado de baja calidad
 *
 * APROX 7:
 *  combinacion de dos t�cnicas diferentes:
 *   - aprox 1 cuando no se conoce la posicion de la pelota
 *   - aprox 6 cuando si se conoce la posicion de la pelota
 * 
 */

#ifndef ALGORITMOII_H_
#define ALGORITMOII_H_


#include "image.h"

//#define ALGORITMOII_H_VERBOSE
//#define ALGORITMOII_H_DEBUG

namespace image{
namespace filter{


typedef struct{
	unsigned int px;
	unsigned int py;
	unsigned int peso;
	unsigned int up;
	unsigned int down;
	unsigned int right;
	unsigned int left;
}tParticula;


void umbralizar(ImagenRGB *imagen, 
	bool(*pfuncionTest)(const rgb_color* pixel),
	rgb_color positive,
	rgb_color negative
);


void luckyCenter(tParticula *particula, 
	const ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int w_min, unsigned int h_min,
	unsigned int w_max, unsigned int h_max,
	bool(*pfuncionTest)(const rgb_color* pixel)
);

inline void luckyCenter(tParticula *particula, 
	const ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	bool(*pfuncionTest)(const rgb_color* pixel)
){
	luckyCenter(particula, imagen, w_step,h_step, 0,0, imagen->WIDTH,imagen->HEIGHT, pfuncionTest);
}

void paintNet(ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int w_min, unsigned int h_min,
	unsigned int w_max, unsigned int h_max,
	rgb_color color
);

inline void paintNet(ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	rgb_color color
){
	paintNet(imagen, w_step, h_step,0,0,imagen->WIDTH,imagen->HEIGHT,color);
}

void searchBest(tParticula *particula, 
	const ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int c_ancho, unsigned int c_alto,
	bool(*pfuncionTest)(const rgb_color* pixel),
	unsigned int grow_step=0, unsigned int p_pn_relation=0
);

bool clipParticle(tParticula *particula, unsigned int x, unsigned int y, unsigned int X, unsigned int Y);

bool darPeso(tParticula *particula, const ImagenRGB *imagen, bool(*pfuncionTest)(const rgb_color* pixel));

void growParticle(tParticula *particula, const ImagenRGB *imagen, unsigned int grow_step, unsigned int p_np_relation, bool(*pfuncionTest)(const rgb_color* pixel));
	/*Genera el peso de cada part�cula contenido en un entorno m�nimo de c_alto x c_ancho de dimensiones
	 * crecientes mientras n� puntos caracter�sticos (de imagen) sea mayor que la mitad de n� de puntos
	 * no caracter�sticos . Par�metros:
	 *   poblacion: objeto que va a ser modificado
	 *   imagen: matriz necesaria para asignar un valor v�lido
	 *   c_ancho: ancho del pol�gono que encierra el entorno
	 *   c_alto: alto del pol�gono
	 */

void shrinkParticle(tParticula *particula, const ImagenRGB *imagen, unsigned int shrink_step, unsigned int np_p_relation, bool(*pfuncionTest)(const rgb_color* pixel));
	/*Genera el peso de cada part�cula contenido en un entorno m�nimo de c_alto x c_ancho de dimensiones
	 * crecientes mientras n� puntos caracter�sticos (de imagen) sea mayor que la mitad de n� de puntos
	 * no caracter�sticos . Par�metros:
	 *   poblacion: objeto que va a ser modificado
	 *   imagen: matriz necesaria para asignar un valor v�lido
	 *   c_ancho: ancho del pol�gono que encierra el entorno
	 *   c_alto: alto del pol�gono
	 */


void recuadrarObjeto(ImagenRGB *imagen, const tParticula *particula, void(*pfuncionPintar)(rgb_color* pixel));
	/*Genera un rect�ngulo sobre la imagen 'img_salida' de dimensiones anchoXalto x_max e y_min respectivamente.
	 *   La modificaci�n de la imagen corre a cargo de una funci�n gen�rica 'pfuncionrecuadrar' cuyos par�metros son:
	 *   	'img_salida': la imagen a modificar
	 *   	'px': coordenada x de la imagen
	 *   	'py': coordenada y de la imagen
	 */


}}//namespace

#endif
