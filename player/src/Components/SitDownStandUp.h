#ifndef SitDownStandUp_H
#define SitDownStandUp_H

#include "Component.h"
#include "RFootLongPress.h"
#include "LFootLongPress.h"
#include "Body.h"
#include "TestComp.h"
#include "BumperDetector.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class SitDownStandUp : public Component, public Singleton<SitDownStandUp>
{
public:

	SitDownStandUp();
	~SitDownStandUp();

	void step();
private:

	static const int Initial	= 0;
	static const int Off	= 1;
	static const int On	= 2;
	static const int Down	= 3;
	static const int StandUp	= 4;
	static const int SitDown	= 5;
	int state;

	RFootLongPress *_RFootLongPress;
	LFootLongPress *_LFootLongPress;
	Body *_Body;
	TestComp *_TestComp;
	BumperDetector *_BumperDetector;

	void Initial_state_code(void);
	void Off_state_code(void);
	void On_state_code(void);
	void Down_state_code(void);
	void StandUp_state_code(void);
	void SitDown_state_code(void);
	bool Initial2Off_transition_code(void);
	bool Off2On_RFootLong_transition_code(void);
	bool On2Off_LFootShort_transition_code(void);
	bool Down2StandUp_RFootLong_transition_code(void);
	bool SitDown2Down_KickMotionFinalized_transition_code(void);
	bool StandUp2Off_KickMotionFinalized_transition_code(void);
	bool Off2SitDown_LFootLong_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
private:
	int debug_state;
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

