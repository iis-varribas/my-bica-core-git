#include "algoritmoGoal.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

namespace image{
namespace filterGoal{

void aPolares(tGoalCoord *coord){
    coord->theta= atan2(coord->y,coord->x);
    int x2=pow(coord->x,2);
    int y2=pow(coord->y,2);
    coord->r= sqrt(x2+y2);
}
void searchBestGoal(tGoalCoord *coord1,
        tGoalCoord *coord2,
	const ImagenRGB *imagen,
	bool(*pfuncionTest)(const rgb_color* pixel))
{
        memset(coord1, 0, sizeof(tGoalCoord));
        memset(coord2, 0, sizeof(tGoalCoord));
	unsigned int w_len, h_len;
        int VA1=-1;	
        int VA2=-1;
        bool antA= false; //Si el anterior era amarillo
        int pmedios1[(imagen->HEIGHT)]; //array para puntos medios del primer poste
        int pmedios2[(imagen->HEIGHT)]; //array para puntos medios del segundo poste
        int N1=0;
        int N2=0;
        int media1=-1;
        int media2=-1;
        bool encontrado1=false;
        bool encontrado2= false;
        int k=(imagen->HEIGHT)-1;
        int transicion;
        //El origen de coordenadas esta en la esquina superior izquierda y
        // aumenta hacia abajo-derecha.
	for (int h=0; h<(imagen->HEIGHT); h++){
            VA1=-1; //Reinicia en cada linea
            VA2=-1;
            
            pmedios1[h]=-1; 
            pmedios2[h]=-1; 
            //printf("linea nueva:%d\n",h);
            antA=false;
            transicion=1;
            for (int w=0; w<(imagen->WIDTH); w++){
                        //Comprueba que el pixel sea del color indicado
			if ( pfuncionTest(&(imagen->pixel[h][w])) ){
                            
                            if (!antA){//Si el pixel anterior no era amarillo
                                if(transicion==1){
                                //Si es la primera transicion de no amarillo a
                                // amarillo, guarda la x en VA1 y cambia antA.
                                VA1=w;
                                //printf("VA1:%d ",VA1);
                                transicion=2;//Ahora espera un pixel no amarillo
                                }else{
                                    if(transicion==3){
                                        //Si el anterior no era amarillo y es la segunda
                                        //transicion de no amarillo a amarillo
                                        VA2=w;
                                        //printf("VA2:%d ",VA2);
                                        transicion=4;
                                    }
                                }
                                antA=true;
                            }//Si el anterior era amarillo no hace nada                            
                        }else{ //Si el pixel no es amarillo
                            
                            if(antA){//Si el anterior si lo era
                                
                                if(transicion==2){
                                    
                                        //si el anterior era amarillo y no ha habido
                                        // transicion amarillo a no amarillo aun.
                                        pmedios1[h]=(w+VA1)/2; //Calculo del primer poste encontrado
                                        media1+=pmedios1[h];
                                        N1++;
                                        //printf("AV1:%d\n",w);
                                        //printf(" pto medio1:%d\n",pmedios1[h]);
                                        transicion=3;
                                        
                                }else{
                                        if(transicion==4){
                                                //si el anterior era amarillo y no ha habido
                                                // transicion amarillo a no amarillo aun.
                                                pmedios2[h]=(w+VA2)/2; //Calculo del segundo poste encontrado
                                                media2+=pmedios2[h];
                                                N2++;
                                                //printf("AV2:%d\n",w);
                                                //printf(" pto medio2:%d\n",pmedios2[h]);
                                        }
                                }
                                antA=false;
                            }//Si no es amarillo y el anterior no era amarillo: Nada
                        }
		}
	}
        //printf("/nFIN\n\n");
        //Calcula la media
        if (N1!=0){
            media1=media1/N1;
        }else encontrado1=true; //No hay
        
        if (N2!=0){
            media2=media2/N2;
        }else encontrado2=true;
        
        coord1->x=-1; //Por si no se encuentra ninguno.
        coord1->y=-1;
        coord2->x=-1;
        coord2->y=-1;
        
        while(k>=0 && !(encontrado1 && encontrado2)){
            if(!encontrado1 && pmedios1[k]<=media1 && pmedios1[k]>=0){
                coord1->x= pmedios1[k];
                coord1->y=k;
                encontrado1=true;
            }
            if(!encontrado2 && pmedios2[k]<=media2 && pmedios2[k]>=0){
                coord2->x= pmedios1[k];
                coord2->y=k;
                encontrado2=true;
            }
            k--;
        }
        /*
        for(int z=0;z<(imagen->HEIGHT);z++){
            if(pmedios1[z]>=0){
                printf("pmedios final x1:%d y1:%d\n",pmedios1[z],z);
            }
            if(pmedios2[z]>=0){
                printf("pmedios final x2:%d y2:%d\n\n",pmedios2[z],z);
            }
        }
        */
        
        printf("\n\nCENTRALES\n");
        //printf("MAXIMO ARRAY WIDTH:%d ",(imagen->WIDTH));
        //printf("MAXIMO ARRAY HEIGHT:%d ",(imagen->HEIGHT));
        printf("MEDIA x1:%d MEDIA x2:%d\n",media1,media2);
        if(coord1->x>=0){
            printf("x1:%d y1:%d\n",coord1->x,coord1->y);
            if (coord2->x>=0){
                printf("x2:%d y2:%d\n\n",coord2->x,coord2->y);
            }
        }else printf("No detectado\n");
}
}}//namespace

