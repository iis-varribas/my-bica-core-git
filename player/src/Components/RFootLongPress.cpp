#include "RFootLongPress.h"

RFootLongPress::RFootLongPress() {
	setTimeForActive(SECONDS*1000000);
	_BumperDetector = BumperDetector::getInstance();
        
        this->setFreqTime(_BumperDetector->getFreqTime());
}



bool 
RFootLongPress::testFunction(void){
	bool Ll,Lr,Rl,Rr;

	_BumperDetector->step();
	_BumperDetector->getValues(Ll,Lr,Rl,Rr);

	return Rr;
}

