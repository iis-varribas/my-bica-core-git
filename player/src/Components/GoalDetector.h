#ifndef GoalDetector_H
#define GoalDetector_H

#include "Component.h"
#include "CameraRGB.h"
#include "Kinematics.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "ImageInput.h"
#include "image.h"
#include "algoritmoGoal.h"
//#include "Debug.h" // class : public DebugIImg

//#define PAINT_NET
//#define VERBOSE
//#define BALL_DEBUG
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end


class GoalDetector : public Component, public Singleton<GoalDetector>//, public DebugIImg //, public DebugIAbs, public DebugIRel
{
public:

	GoalDetector();
	~GoalDetector();

	void step();
private:

	static const int Initial	= 0;
	static const int NotDetected	= 1;
	static const int Detected	= 2;
	int state;

	CameraRGB *_CameraRGB;
	Kinematics *_Kinematics;

	void Initial_state_code(void);
	void NotDetected_state_code(void);
	void Detected_state_code(void);
	bool Initial2NotDetected0_transition_code(void);
	bool NotDetected2Detected0_transition_code(void);
	bool Detected2NotDetected0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
        //set HSV color limits. Pixeles between [hsv_min, hsv_max] are treated as positives
        
//        bica::ShapeList getGrDebugImg();
//        bica::ShapeList getGrDebugAbs();
//        bica::ShapeList getGrDebugRel();

public:

        //search algorithm parameters

private:
	image::ImagenRGB imagenRGB;
        static image::hsv_color hsv_min;
	static image::hsv_color hsv_max;
        
        image::filterGoal::tGoalCoord coord1;
        image::filterGoal::tGoalCoord coord2;
        
        image::filterGoal::tGoalCoord poste1;
        image::filterGoal::tGoalCoord poste2;
        image::filterGoal::tGoalCoord poste3;
        image::filterGoal::tGoalCoord poste4;
        
        
        //Para calculo de varianza
        double varR;
        double varT;
        
	static bool funcionTest(const image::rgb_color *pixel);
        void getImagePosition(image::filterGoal::tGoalCoord *coord,HPoint2D &p2d); //Image coordinates system
	void get2DPosition(image::filterGoal::tGoalCoord *coord,HPoint2D &p2d); //Kinematics 2D coords system
	void get3DPosition(image::filterGoal::tGoalCoord *coord,HPoint3D &p3d); //Kinematics 3D coords system
        
	
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

