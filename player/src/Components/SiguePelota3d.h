#ifndef SiguePelota3d_H
#define SiguePelota3d_H

#include "Component.h"
#include "NewBallDetector.h"
#include "Kinematics.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "head/head/Head.h"  //remove "Head.h" include
#include "progeo.h"
#include "discretemux.h"
#include "GoToBall.h"

//#define USE_EXPECTED 100
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class SiguePelota3d : public Component, public Singleton<SiguePelota3d>
{
public:

	SiguePelota3d();
	~SiguePelota3d();

	void step();
private:

	static const int Initial	= 0;
	static const int RelativeMove	= 1;
	static const int WaitForDetect	= 2;
	static const int DirectMove	= 3;
	int state;

	Head *_Head;
	NewBallDetector *_NewBallDetector;
	Kinematics *_Kinematics;

	void Initial_state_code(void);
	void RelativeMove_state_code(void);
	void WaitForDetect_state_code(void);
	void DirectMove_state_code(void);
	bool Initial2WaitForDetect_transition_code(void);
	bool RelativeMove2WaitForDetect0_transition_code(void);
	bool WaitForDetect2DirectMove0_transition_code(void);
	bool DirectMove2WaitForDetect0_transition_code(void);
	bool WaitForDetect2RelativeMove0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
        
public:
        //Velocity multipliers
        static const float PAN_K = 0.8;
        static const float TILT_K = 0.9;
        
        //Limits of relaxed region. Head will stand within this region.
        //This values are measured in pixels
        static const unsigned int PAN_L = 15;
        static const unsigned int TILT_L = 15;
        
private:
        HPoint3D ball3d;
        HPoint2D ball2d;
        HPoint2D center2d;
        
        bool directMoveDone;
        float _panPos, _tiltPos;

//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

