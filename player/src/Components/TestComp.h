#ifndef TestComp_H
#define TestComp_H

#include "Component.h"
#include "USDetector.h"
#include "Body.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "progeo.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class TestComp : public Component, public Singleton<TestComp>
{
public:

	TestComp();
	~TestComp();

	void step();
private:

	static const int Initial	= 0;
	static const int Adelante	= 1;
	static const int Obstaculo	= 2;
	int state;

	USDetector *_USDetector;
	Body *_Body;

	void Initial_state_code(void);
	void Adelante_state_code(void);
	void Obstaculo_state_code(void);
	bool Initial2Adelante0_transition_code(void);
	bool Adelante2Obstaculo0_transition_code(void);
	bool Obstaculo2Adelante0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

