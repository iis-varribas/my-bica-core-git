/*
 * Name: ButtonDetector.cpp
 * @Author: Alejandro Aporta (a.aporta@alumnos.urjc.es)
 * @Author: Francisco Martín (fmartin@gsyc.es)
 
 * 
 * Description: Class to detect other robots
 *
 * Created on: 04/11/2011
 *
 * Copyright (C) Universidad Rey Juan Carlos
 * All Rights Reserved.
 *
 */

#include "ButtonDetector.h"
 

ButtonDetector::ButtonDetector()
{
	left = right = chest = false;
	cont = 0;
}

ButtonDetector::~ButtonDetector()
{
}

void
ButtonDetector::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker) {
	Component::init(newName, parentBroker);
	
	setFreqTime(LONG_RATE);
}


void
ButtonDetector::getValues(bool &rleft, bool &rright, bool &rchest)
{
	rleft = left;
	rright = right;
	rchest = chest;
}

void
ButtonDetector::step()
{
	
	if (!isTime2Run())
		return;

	if(right || left || chest)
		cont++;
	if(cont>1)
	{
		left = right = chest = false;
		cont = 0;
	}

	cerr<<"[";
	if(left) cerr<<"*";
		else cerr<<"-";
	if(chest) cerr<<"*";
		else cerr<<"-";
	if(right) cerr<<"*";
		else cerr<<"-";
	cerr<<"]"<<endl;
}

void
ButtonDetector::RightBumperPressed()
{
	right = true;
}

void
ButtonDetector::LeftBumperPressed()
{
	left = true;
}

void
ButtonDetector::ChestButtonPressed()
{
	chest = true;
}




