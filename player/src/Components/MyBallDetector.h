#ifndef MYBALLDETECTOR_H_
#define MYBALLDETECTOR_H_

#include "Detector.h"
#include "CameraRGB.h"
#include "Kinematics.h"
#include "Debug.h"


#include "image.h"
#include "forms.h"
#include "algoritmoII.h"


class CameraRGB;

class MyBallDetector: public Component, public Singleton<MyBallDetector>, public DebugIImg
{
public:
	MyBallDetector();
	~MyBallDetector();

	void step();
	
	bica::ShapeList getGrDebugImg();

private:
	void waitForColor();

	void blackWhiteBall();
	void blackNoBall();
	void shapeBall();
	void luckyShapeBall();
	void luckyShapeBall2();
	void bestAndluckyShapeBall();

	void formsDebug();
	void hsvDebug();

private:

	void detect();

	CameraRGB		*_CameraRGB;
	Kinematics 		*_Kinematics;

//	image::t_rgb rgbImg[ImageInput::IMG_HEIGHT][ImageInput::IMG_WIDTH];
	image::ImagenRGB imagenRGB;
	image::filter::tParticula particula;
};

#endif /* MYBALLDETECTOR_H_ */
