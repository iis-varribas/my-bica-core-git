#ifndef ALGORITMOGOAL_H_
#define ALGORITMOGOAL_H_


#include "image.h"
#include "Kinematics.h"

//#define ALGORITMOGOAL_H_VERBOSE
//#define ALGORITMOGOAL_H_DEBUG
namespace image{
namespace filterGoal{

typedef struct{
        int x;
	int y;
        double r;
        double theta;
}tGoalCoord;

void aPolares(tGoalCoord *coord);
void searchBestGoal(tGoalCoord *coord1,
        tGoalCoord *coord2,
	const ImagenRGB *imagen,
	bool(*pfuncionTest)(const rgb_color* pixel)
);
}}//namespace

#endif
