# Libname
set(libname head)

# Include ALCOMMON libraries
find_package(alcommon)

# Include (local) directories to compile
include_directories(".")
include_directories(${ALCOMMON_INCLUDE_DIRS})

# Pass include directories to parent
set(head_include_directories ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)

# Create library
add_library(${libname}
	STATIC
	head/Head.cpp
	src/JointControl.cpp
)
set(head_libs ${libname} PARENT_SCOPE)

# Add tests
add_subdirectory(test)
