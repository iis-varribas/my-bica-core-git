#ifndef _FORMS_H_
#define _FORMS_H_

#include "image.h"


namespace image{
namespace forms{

bool square(image::ImagenRGB &img, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage);

bool rhombus(image::ImagenRGB &img, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage);

bool circle(image::ImagenRGB &img, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage);

}}
#endif
