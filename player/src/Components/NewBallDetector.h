#ifndef NewBallDetector_H
#define NewBallDetector_H

#include "Component.h"
#include "CameraRGB.h"
#include "Kinematics.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "ImageInput.h"
#include "image.h"
#include "algoritmoII.h"
#include "Debug.h" // class : public DebugIImg

#define PAINT_NET
//#define VERBOSE
//#define BALL_DEBUG
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end


typedef struct ball2d_instant{
	unsigned int ph;
	unsigned int pw;
	unsigned long time;
}ball2d_instant;

typedef struct ball2d_velocity{
	int vel_h;
	int vel_w;
	ball2d_instant now;
	ball2d_instant old;
}ball2d_velocity;


typedef struct ball3d_instant{
        HPoint3D pos;
	unsigned long time;
}ball3d_instant;

typedef struct ball3d_velocity{
	HPoint3D vel;
	ball3d_instant now;
	ball3d_instant old;
}ball3d_velocity;
	

class NewBallDetector : public Component, public Singleton<NewBallDetector>, public DebugIImg //, public DebugIAbs, public DebugIRel
{
public:

	NewBallDetector();
	~NewBallDetector();

	void step();
private:

	static const int Initial	= 0;
	static const int NotDetected	= 1;
	static const int Detected	= 2;
	int state;

	CameraRGB *_CameraRGB;
	Kinematics *_Kinematics;

	void Initial_state_code(void);
	void NotDetected_state_code(void);
	void Detected_state_code(void);
	bool Initial2NotDetected0_transition_code(void);
	bool NotDetected2Detected0_transition_code(void);
	bool Detected2NotDetected0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
        //set HSV color limits. Pixeles between [hsv_min, hsv_max] are treated as positives
	void setBallColors(image::hsv_color hsv_min, image::hsv_color hsv_max);
        
	bool isDetected();
        
	void getImagePosition(HPoint2D &p2d); //Image coordinates system
	void get2DPosition(HPoint2D &p2d); //Kinematics 2D coords system
	void get3DPosition(HPoint3D &p3d); //Kinematics 3D coords system
        
        void getExpectedImagePosition(HPoint2D &p2d, long millis); //Image coordinates system
        void getExpected3DPosition(HPoint3D &p3d, long millis); //Kinematics 3D coords system
        
        bica::ShapeList getGrDebugImg();
//        bica::ShapeList getGrDebugAbs();
//        bica::ShapeList getGrDebugRel();

public:
        //ball geometry
        static const float BALL_RADIUS = 32.5;//millimeters

        //search algorithm parameters
	static const unsigned int not_found_step = 5;
	static const unsigned int not_found_size = 5;
	static const unsigned int found_step_min = 1;
	static const unsigned int found_step_max = 1;

	//padding increment percentage
	static const int static_padding = 0;
	static const float dynamic_padding = 1.5;

private:
	void calcule2DBallMovement();
        void calcule3DBallMovement();
        long getTimeMilis();

private:
	image::ImagenRGB imagenRGB;
	image::filter::tParticula particula;

	ball2d_velocity ball2d;
        ball3d_velocity ball3d;

	static bool funcionTest(const image::rgb_color *pixel);
	static image::hsv_color hsv_min;
	static image::hsv_color hsv_max;
/*
	WEBOTS
	HSV min=[14 208 162]
	HSV max=[65,255,255]

	ROBOT --UNDEF--
	HSV min=[14 208 162]
	HSV max=[65,255,255]
*/

	int ancho,alto;

//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

