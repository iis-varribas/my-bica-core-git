#include "BallSearch.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

BallSearch::BallSearch()
{
	_NewBallDetector = NewBallDetector::getInstance();
	_Head = Head::getInstance();
	_Kinematics = Kinematics::getInstance();
	_Body = Body::getInstance();
	state = Initial;
}

BallSearch::~BallSearch()
{

}

void BallSearch::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
    encontrada=false;
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void BallSearch::Search_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Search_state_code
    change=0;
    encontrada=false;
    _Head->setPan(-0.6);
//BUILDER COMMENT. DO NOT REMOVE. end Search_state_code
}

void BallSearch::Moving_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Moving_state_code
}

void BallSearch::Found_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Found_state_code
        _Head->stop();
        encontrada=true;
        change=0;
        _Body->setVel(0,0,0);
//BUILDER COMMENT. DO NOT REMOVE. end Found_state_code
}

void BallSearch::LeftScan_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin LeftScan_state_code
printf("CAMBIO A LA IZQUIERDA\n");
_Body->setVel(0,0,0);
_Head->setTiltPos((3.14/2),1);
change=0;
_Head->setPan(-0.4);

//BUILDER COMMENT. DO NOT REMOVE. end LeftScan_state_code
}

void BallSearch::RightScanPan_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin RightScanPan_state_code
printf("CAMBIO A LA DERECHA\n");
_Head->setTiltPos(-0.2,1); //Sube
_Head->setPan(0.4);
change=1;
printf("Activando cuerpo\n");
_Body->setVel(0,1,0);
printf("cuerpo activado\n");
//BUILDER COMMENT. DO NOT REMOVE. end RightScanPan_state_code
}

bool BallSearch::Search2Moving0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Search2Moving0_transition_code
return _NewBallDetector->isDetected()==false;
//BUILDER COMMENT. DO NOT REMOVE. end Search2Moving0_transition_code
}

bool BallSearch::LeftScan2Found0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin LeftScan2Found0_transition_code
return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end LeftScan2Found0_transition_code
}

bool BallSearch::Moving2Found0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving2Found0_transition_code
return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end Moving2Found0_transition_code
}

bool BallSearch::Search2Found0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Search2Found0_transition_code
return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end Search2Found0_transition_code
}

bool BallSearch::Found2Search0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Found2Search0_transition_code
return !(_NewBallDetector->isDetected());
//BUILDER COMMENT. DO NOT REMOVE. end Found2Search0_transition_code
}

bool BallSearch::LeftScan2Moving0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin LeftScan2Moving0_transition_code
return !(_NewBallDetector->isDetected());
//BUILDER COMMENT. DO NOT REMOVE. end LeftScan2Moving0_transition_code
}

bool BallSearch::RightScanPan2Moving0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin RightScanPan2Moving0_transition_code
return !(_NewBallDetector->isDetected());
//BUILDER COMMENT. DO NOT REMOVE. end RightScanPan2Moving0_transition_code
}

bool BallSearch::RightScanPan2Found0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin RightScanPan2Found0_transition_code
return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end RightScanPan2Found0_transition_code
}

bool BallSearch::Moving2RightScanPan0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving2RightScanPan0_transition_code
_Kinematics->cloneKinematics(&myKinematics);
return ((myKinematics.pan>1.2) && (change==0)); // Right Scan
//BUILDER COMMENT. DO NOT REMOVE. end Moving2RightScanPan0_transition_code
}

bool BallSearch::Moving2LeftScan0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving2LeftScan0_transition_code
_Kinematics->cloneKinematics(&myKinematics);
return (myKinematics.pan<-1.2 && (change==1));//Left Scan
//BUILDER COMMENT. DO NOT REMOVE. end Moving2LeftScan0_transition_code
}

bool BallSearch::Initial2Found0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2Found0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2Found0_transition_code
}

void
BallSearch::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2Found0_transition_code()) {
				state = Found;
			}
		}

		break;
	case Search:
		_NewBallDetector->step();

		if (isTime2Run()) {
			Search_state_code();

			if (Search2Found0_transition_code()) {
				state = Found;
			}
			else if (Search2Moving0_transition_code()) {
				state = Moving;
			}
		}

		_Head->step();
		break;
	case Moving:
		_Kinematics->step();
		_NewBallDetector->step();

		if (isTime2Run()) {
			Moving_state_code();

			if (Moving2Found0_transition_code()) {
				state = Found;
			}
			else if (Moving2RightScanPan0_transition_code()) {
				state = RightScanPan;
			}
			else if (Moving2LeftScan0_transition_code()) {
				state = LeftScan;
			}
		}

		_Head->step();
		break;
	case Found:
		_NewBallDetector->step();

		if (isTime2Run()) {
			Found_state_code();

			if (Found2Search0_transition_code()) {
				state = Search;
			}
		}

		_Body->step();
		_Head->step();
		break;
	case LeftScan:
		_Kinematics->step();
		_NewBallDetector->step();

		if (isTime2Run()) {
			LeftScan_state_code();

			if (LeftScan2Found0_transition_code()) {
				state = Found;
			}
			else if (LeftScan2Moving0_transition_code()) {
				state = Moving;
			}
		}

		_Body->step();
		_Head->step();
		break;
	case RightScanPan:
		_NewBallDetector->step();
		_Kinematics->step();

		if (isTime2Run()) {
			RightScanPan_state_code();

			if (RightScanPan2Moving0_transition_code()) {
				state = Moving;
			}
			else if (RightScanPan2Found0_transition_code()) {
				state = Found;
			}
		}

		_Head->step();
		_Body->step();
		break;
	default:
		cout << "[BallSearch::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
bool BallSearch::getEncontrada(){
    return encontrada;
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

