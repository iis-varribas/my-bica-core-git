#ifndef TestLongActivityAbstractComponent_H
#define TestLongActivityAbstractComponent_H

#include "Component.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include <sys/time.h>
//#include "altimeval.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class TestLongActivityAbstractComponent : public Component
{
public:
	TestLongActivityAbstractComponent();
	~TestLongActivityAbstractComponent();

	void step();
private:

	static const int Initial	= 0;
	static const int False	= 1;
	static const int True	= 2;
	static const int Active	= 3;
	int state;


	void Initial_state_code(void);
	void False_state_code(void);
	void True_state_code(void);
	void Active_state_code(void);
	bool Initial2False0_transition_code(void);
	bool False2True0_transition_code(void);
	bool True2False0_transition_code(void);
	bool Active2False0_transition_code(void);
	bool True2Active0_transition_code(void);
	bool False2Active0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);

	void setTimeForActive(unsigned long milis);
	void getValues(bool &data);
	bool isActived();
private:
	bool ACTIVED;
	bool test_value;
	unsigned long activation_threshold;
	struct timeval tv_ini, tv_now;

	virtual bool testFunction(void)=0;
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

