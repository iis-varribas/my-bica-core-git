#include "NewBallDetector.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

NewBallDetector::NewBallDetector()
{
	_CameraRGB = CameraRGB::getInstance();
	_Kinematics = Kinematics::getInstance();
	state = Initial;
        
        this->setFreqTime(_CameraRGB->getFreqTime());
//        setFreqTime(ImageInput::CAPTURE_RATE);
}

NewBallDetector::~NewBallDetector()
{

}

void NewBallDetector::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code    
#ifdef WEBOTS
    hsv_min.h = 14;
    hsv_min.s = 208;
    hsv_min.v = 162;

    hsv_max.h = 65;
    hsv_max.s = 255;
    hsv_max.v = 255;
#else

// min=[0 61 178]
// max=[176,144,255]

    hsv_min.h = 0;
    hsv_min.s = 61;
    hsv_min.v = 178;

    hsv_max.h = 176;
    hsv_max.s = 144;
    hsv_max.v = 255;
#endif
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void NewBallDetector::NotDetected_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin NotDetected_state_code
	startDebugInfo();

	ancho = not_found_step;
	alto  = not_found_step;

	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);

	image::filter::searchBest(
		&particula, 
		&imagenRGB, 
		not_found_step, not_found_step,
		not_found_size, not_found_size,
		funcionTest
	);
	
#ifdef PAINT_NET
	image::rgb_color rgb_net = {0,0,0};
	image::filter::paintNet(
		&imagenRGB,
		not_found_step, not_found_step,
		0, 0,
		imagenRGB.WIDTH, imagenRGB.HEIGHT,
		rgb_net
	);
#endif

	endDebugInfo();
//BUILDER COMMENT. DO NOT REMOVE. end NotDetected_state_code
}

void NewBallDetector::Detected_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Detected_state_code
	startDebugInfo();

	int x_min,y_min, x_max,y_max;

#ifdef VERBOSE
	printf("Founded! %d [%d][%d]/%d:%d {%d,%d,%d,%d}\n", particula.peso,
		particula.px, particula.py, ancho, alto,
		particula.left, particula.right, particula.up, particula.down
	);
#endif

        this->calcule2DBallMovement();
        this->calcule3DBallMovement();

	ancho = (particula.left + particula.right) >> 2; if(ancho < found_step_min) ancho = found_step_min; else if(ancho > found_step_max) ancho = found_step_max;
	alto  = (particula.up   + particula.down)  >> 2; if(alto  < found_step_min) alto  = found_step_min; else if(alto  > found_step_max) alto  = found_step_max;

	x_min = (particula.px + ball2d.vel_w) - (static_padding + (1+dynamic_padding)*particula.left);   if (x_min < 0) x_min = 0;
	y_min = (particula.py + ball2d.vel_h) - (static_padding + (1+dynamic_padding)*particula.up + 1); if (y_min < 0) y_min = 0;

	x_max = (particula.px + ball2d.vel_w) + (static_padding + (1+dynamic_padding)*particula.right);    if (x_max > imagenRGB.WIDTH ) x_max = imagenRGB.WIDTH;
	y_max = (particula.py + ball2d.vel_h) + (static_padding + (1+dynamic_padding)*particula.down + 1); if (y_max > imagenRGB.HEIGHT) y_max = imagenRGB.HEIGHT;


	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);

	image::filter::luckyCenter(
		&particula, 
		&imagenRGB, 
		ancho,alto,
		x_min,y_min,
		x_max,y_max,
		&funcionTest
	);

#ifdef PAINT_NET
		image::rgb_color rgb_net = {0,0,0};
		image::filter::paintNet(
			&imagenRGB,
			ancho,alto,
			x_min,y_min,
			x_max,y_max,
			rgb_net
		);
#endif
#ifdef VERBOSE
	HPoint3D p3d;
	memset(&p3d, 0, sizeof(HPoint3D));
	this->get3DPosition(p3d);
	printf("3D x:%.2f y:%.2f z:%.2f\n", p3d.X/p3d.H, p3d.Y/p3d.H, p3d.Z/p3d.H);
#endif

	endDebugInfo();
//BUILDER COMMENT. DO NOT REMOVE. end Detected_state_code
}

bool NewBallDetector::Initial2NotDetected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2NotDetected0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2NotDetected0_transition_code
}

bool NewBallDetector::NotDetected2Detected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin NotDetected2Detected0_transition_code
if (particula.peso > 0){
	ball2d.now.time = getTimeMilis();
	ball2d.now.ph = particula.py;
	ball2d.now.pw = particula.px;
        
        ball3d.now.time = ball2d.now.time;
        get3DPosition(ball3d.now.pos);
        
	return true;
}
return false;
//BUILDER COMMENT. DO NOT REMOVE. end NotDetected2Detected0_transition_code
}

bool NewBallDetector::Detected2NotDetected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Detected2NotDetected0_transition_code
return particula.peso == 0;
//BUILDER COMMENT. DO NOT REMOVE. end Detected2NotDetected0_transition_code
}

void
NewBallDetector::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2NotDetected0_transition_code()) {
				state = NotDetected;
			}
		}

		break;
	case NotDetected:
		_CameraRGB->step();

		if (isTime2Run()) {
			NotDetected_state_code();

			if (NotDetected2Detected0_transition_code()) {
				state = Detected;
			}
		}

		break;
	case Detected:
		_CameraRGB->step();
		_Kinematics->step();

		if (isTime2Run()) {
			Detected_state_code();

			if (Detected2NotDetected0_transition_code()) {
				state = NotDetected;
			}
		}

		break;
	default:
		cout << "[NewBallDetector::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin

image::hsv_color NewBallDetector::hsv_min;
image::hsv_color NewBallDetector::hsv_max;

bool
NewBallDetector::funcionTest(const image::rgb_color *pixel){
	image::hsv_color hsv;
	image::rgb2hsv(*pixel, hsv);
	if (	   (hsv.h >= hsv_min.h && hsv.h <= hsv_max.h)
		&& (hsv.s >= hsv_min.s && hsv.s <= hsv_max.s)
		&& (hsv.v >= hsv_min.v && hsv.v <= hsv_max.v)
	){
		return true;
	}else{
		return false;
	}
}




void 
NewBallDetector::setBallColors(image::hsv_color hsv_min, image::hsv_color hsv_max){
	this->hsv_min = hsv_min;
	this->hsv_max = hsv_max;
}

bool 
NewBallDetector::isDetected(){
	return state == Detected && particula.peso > 0;
}

void 
NewBallDetector::getImagePosition(HPoint2D &p2d){
	p2d.x = particula.px;
	p2d.y = particula.py;
	p2d.h = 1.0;
}

void 
NewBallDetector::get2DPosition(HPoint2D &p2d){
	this->getImagePosition(p2d);
	_Kinematics->pixel2Optical(_Kinematics->getCamera(), &p2d);
}

void 
NewBallDetector::get3DPosition(HPoint3D &p3d){
	HPoint2D p2d;

	this->getImagePosition(p2d);
	_Kinematics->get3DPositionZ(p3d, p2d, BALL_RADIUS);
	if (p3d.H != 1.0) {
		p3d.X /= p3d.H;
		p3d.Y /= p3d.H;
		p3d.Z /= p3d.H;
		p3d.H = 1.0;
	}
}

void 
NewBallDetector::getExpectedImagePosition(HPoint2D &p2d, long millis)
{
    p2d.h = 1;
    p2d.x = particula.px + ball2d.vel_w * millis / (ball2d.now.time - ball2d.old.time);
    p2d.y = particula.py + ball2d.vel_h * millis / (ball2d.now.time - ball2d.old.time);
}

void 
NewBallDetector::getExpected3DPosition(HPoint3D &p3d, long millis)
{
    p3d = ball3d.now.pos;
    p3d.X += ball3d.vel.X * millis;
    p3d.Y += ball3d.vel.Y * millis;
    p3d.Z += ball3d.vel.Z * millis;
}

long 
NewBallDetector::getTimeMilis(){
        return this->getCurrentTime()/*microsegundos*/ / 1000; //milisegundos
}


void 
NewBallDetector::calcule2DBallMovement(){
        ball2d.old = ball2d.now;
	ball2d.now.pw = particula.px;
	ball2d.now.ph = particula.py;
	ball2d.now.time = getTimeMilis();

//	long time_diff = ball2d.now.time - ball2d.old.time;

	//velocidad medida en pixeles/step
	ball2d.vel_h = (ball2d.now.ph - ball2d.old.ph);
	ball2d.vel_w = (ball2d.now.pw - ball2d.old.pw);
        // ball2d.vel_h / time_diff --> pixeles/milisegundo

#ifdef BALL_DEBUG
       
	printf("Ball 2D vel:\n h=%d\n w=%d\ntime_diff=%ld\n",
		ball2d.vel_h, ball2d.vel_w, time_diff
	);
#endif 
}


void 
NewBallDetector::calcule3DBallMovement(){
	ball3d.old = ball3d.now;
        get3DPosition(ball3d.now.pos);
	ball3d.now.time = getTimeMilis();

	long time_diff = ball3d.now.time - ball3d.old.time;

        //calculo acumulado de velocidad
	//velocidad medida en milimetros/milisegundo
	ball3d.vel.X += (ball3d.now.pos.X - ball3d.old.pos.X) / time_diff;
        ball3d.vel.Y += (ball3d.now.pos.Y - ball3d.old.pos.Y) / time_diff;
        ball3d.vel.Z += (ball3d.now.pos.Z - ball3d.old.pos.Z) / time_diff;
        ball3d.vel.H = 1;

        ball3d.vel.X /= 2;
        ball3d.vel.Y /= 2;
        ball3d.vel.Z /= 2;
        
#ifdef BALL_DEBUG
	printf("Ball 3D vel:\n x=%f\n y=%f\n z=%f\n",
		ball3d.vel.X, ball3d.vel.Y, ball3d.vel.Z
	);
#endif 
}


bica::ShapeList 
NewBallDetector::getGrDebugImg()
{
	shapeListImg.clear();		

	bica::ImagePtr image(new bica::Image);
	image->pixelData.resize(imagenRGB.SIZE);
	image->width = imagenRGB.WIDTH;
	image->height = imagenRGB.HEIGHT;
	image->color = bica::RED;
	image->filled = false;
	image->opacity = 125;
	image->accKey = "b";
	image->label = "";
	memmove( &(image->pixelData[0]), (char *) imagenRGB.pixel, imagenRGB.SIZE);
	shapeListImg.push_back(image);

	if (particula.peso > 0){

		bica::Point3DPtr p1(new bica::Point3D);
		bica::Point3DPtr p2(new bica::Point3D);
		bica::RectanglePtr r(new bica::Rectangle);
	
		p1->x = particula.px-particula.left;
		p1->y = particula.py-particula.up;
		p1->z = 0;
		p2->x = particula.px+particula.right;
		p2->y = particula.py+particula.down;
		p2->z = 0.0;

		r->p1 = p1;
		r->p2 = p2;
		r->color = bica::PURPLE;
		r->filled = false;
		r->opacity = 125;
		r->accKey = "b";
		r->label = "";
                
                shapeListImg.push_back(r);
                
                bica::ArrowPtr arrow(new bica::Arrow);
                p1 = new bica::Point3D();
                p2 = new bica::Point3D();
                
                HPoint2D p2d;
                HPoint3D p3d;
                getExpected3DPosition(p3d, 1000);
                _Kinematics->get2DPosition(p3d, p2d);
                
                p1->x = particula.px;
                p1->y = particula.py;
                p1->z = 0;
                
                p2->x = p2d.x;
                p2->y = p2d.y;
                p2->z = 0;
                
                arrow->src = p1;
                arrow->dst = p2;
                arrow->width = 5;
                arrow->color = bica::BLUE;
                arrow->opacity = 125;
                arrow->filled = false;
                arrow->accKey = "b";
		arrow->label = "";

		shapeListImg.push_back(arrow);
	}

	return shapeListImg;
}

/*
bica::ShapeList 
NewBallDetector::getGrDebugAbs()
{
    return shapeListAbs;
}


bica::ShapeList 
NewBallDetector::getGrDebugRel()
{
    return shapeListRel;
}
*/
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

