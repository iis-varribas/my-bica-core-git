#ifndef USDMean_H_
#define USDMean_H_

#include "Component.h"
#include "Singleton.h"

#include "USDetector.h"



class USDMean: public Component, public Singleton<USDMean>{
public:
	USDMean();
	~USDMean();

	void step();
	void getValues(HPoint3D &rleft, HPoint3D &rright);


private:
	void loadValues();
	void calc(int n);

private:
	static const int SIZE = 4;

	int pos;
	HPoint3D v_left[SIZE];
	HPoint3D v_right[SIZE];

	HPoint3D left;
	HPoint3D right;

	USDetector* _USDetector;

	int state;
};

#endif
