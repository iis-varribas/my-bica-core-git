#include "TestComp.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

TestComp::TestComp()
{
	_USDetector = USDetector::getInstance();
	_Body = Body::getInstance();
	state = Initial;
}

TestComp::~TestComp()
{

}

void TestComp::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void TestComp::Adelante_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Adelante_state_code
_Body->setVel(1.0, 0.0, 0.0);
//BUILDER COMMENT. DO NOT REMOVE. end Adelante_state_code
}

void TestComp::Obstaculo_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Obstaculo_state_code
_Body->setVel(0.0, 0.0, 0.0);
//BUILDER COMMENT. DO NOT REMOVE. end Obstaculo_state_code
}

bool TestComp::Initial2Adelante0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2Adelante0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2Adelante0_transition_code
}

bool TestComp::Adelante2Obstaculo0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Adelante2Obstaculo0_transition_code
HPoint3D left, right;

_USDetector->getValues(left, right);

if((left.X<300) || (right.X<300)) 
	return true;
else
	return false;
//BUILDER COMMENT. DO NOT REMOVE. end Adelante2Obstaculo0_transition_code
}

bool TestComp::Obstaculo2Adelante0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Obstaculo2Adelante0_transition_code
HPoint3D left, right;

_USDetector->getValues(left, right);

if((left.X>300) && (right.X>300)) 
	return true;
else
	return false;
//BUILDER COMMENT. DO NOT REMOVE. end Obstaculo2Adelante0_transition_code
}

void
TestComp::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2Adelante0_transition_code()) {
				state = Adelante;
			}
		}

		break;
	case Adelante:
		_USDetector->step();

		if (isTime2Run()) {
			Adelante_state_code();

			if (Adelante2Obstaculo0_transition_code()) {
				state = Obstaculo;
			}
		}

		_Body->step();
		break;
	case Obstaculo:
		_USDetector->step();

		if (isTime2Run()) {
			Obstaculo_state_code();

			if (Obstaculo2Adelante0_transition_code()) {
				state = Adelante;
			}
		}

		_Body->step();
		break;
	default:
		cout << "[TestComp::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

