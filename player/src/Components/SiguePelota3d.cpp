#include "SiguePelota3d.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

SiguePelota3d::SiguePelota3d()
{
	_Head = Head::getInstance();
	_NewBallDetector = NewBallDetector::getInstance();
	_Kinematics = Kinematics::getInstance();
	state = Initial;
	resetStopWatch();
        
        this->setFreqTime(_Head->getFreqTime());
}

SiguePelota3d::~SiguePelota3d()
{

}

void SiguePelota3d::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code        
         center2d.x=160;
         center2d.y=120;
         center2d.h=1;
         
         directMoveDone = true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void SiguePelota3d::RelativeMove_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin RelativeMove_state_code
        float pan, tilt;
        float panDest, tiltDest;
        float panNow, tiltNow;
        

        
//        panNow = _Head->getPan();
//        tiltNow = _Head->getTilt();
//        
//        panNow = GoToBall::getInstance()->getValue("HeadYaw");
//        tiltNow = GoToBall::getInstance()->getValue("HeadPitch");
//        
//        pan = (panNow+panDest)/2;
//        tilt = (tiltNow+tiltDest)/2;
//        
//        printf("pan=%.3f [%.3f * %.3f]   tilt=%.3f  [%.3f * %.3f]\n", pan, panNow, panDest, tilt, tiltNow, tiltDest);
        
        
//        pan = panDest;
//        tilt = tiltDest;
//        
//        HPoint2D center2d;
//        center2d.x = 160;
//        center2d.y = 120;
//        center2d.h = 1;
        
//        HPoint3D center3d;
//        _Kinematics->get3DPosition(center3d, center2d);
//        
//        HPoint3D ball3d;
//        _NewBallDetector->get3DPosition(ball3d);
//        
//        //TILT
//        {
//            float value = center3d.X - ball3d.X;
//            printf("diffX=%.3f (%.3f - %.3f)\n", value, center3d.X, ball3d.X);
//            float limits[] = {40, 80, 200, 400}; //millimeters
//            float values[] = {0, 0.1, 0.2, 0.6};
//            tilt = sign(value) * discretemux::getMinValue(abs(value), 1, limits, values, 4);
//        }
//        
//        //PAN
//        {
//            float value = center3d.Y - ball3d.Y;
//            printf("diffY=%.3f (%.3f - %.3f)\n", value, center3d.Y, ball3d.Y);
//            float limits[] = {20, 40, 80, 400}; //millimeters
//            float values[] = {0, 0.1, 0.2, 0.8};
//            pan = sign(value) * discretemux::getMinValue(abs(value), 1, limits, values, 4);
//        }
        
        
      
        //PAN
        {
            float value = ball2d.x - center2d.x;
            float limits[] = {10, 15, 20, 30, 50, 120}; //pixeles
            float values[] = {0, 0.01, 0.05, 0.1, 0.3, 0.5};
            pan = sign(value) * discretemux::getMinValue(abs(value), 1, limits, values, 6);
//            printf("diffX=%.3f (%.3f - %.3f) pan=%.3f\n", value, center2d.x, ball2d.x, pan);
        }
        
        //TILT
        {
            float value = ball2d.y - center2d.y;
            float limits[] = {10, 15, 20, 30, 40, 100}; //pixeles
            float values[] = {0, 0.01, 0.05, 0.1, 0.3, 0.5};
            tilt = sign(value) * discretemux::getMinValue(abs(value), 1, limits, values, 6);
//            printf("diffY=%.3f (%.3f - %.3f) tilt=%.3f\n", value, center2d.y, ball2d.y, tilt);
        }
        
        
        //PAN
        {
            float value = (ball2d.x - center2d.x);
            if (abs(value) < PAN_L){
                pan = 0;
            }else{
                pan = PAN_K*value/center2d.x;
                if (abs(pan) > 1) pan = sign(pan);
            }
//            printf("diffX=%.3f (%.3f - %.3f) pan=%.3f\n", value, center2d.x, ball2d.x, pan);
        }
        
        //TILT
        {
            float value = ball2d.y - center2d.y;
            if (abs(value) < TILT_L){
                tilt = 0;
            }else{
                tilt = TILT_K*value/center2d.y;
                if (abs(tilt) > 1) tilt = sign(tilt);
            }
//            printf("diffY=%.3f (%.3f - %.3f) tilt=%.3f\n", value, center2d.y, ball2d.y, tilt);
        }
        
        _Head->setPan(pan);
	_Head->setTilt(tilt);
        
//        printf("pan=%.3f tilt=%.3f\n", pan, tilt);
//BUILDER COMMENT. DO NOT REMOVE. end RelativeMove_state_code
}

void SiguePelota3d::WaitForDetect_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin WaitForDetect_state_code
//BUILDER COMMENT. DO NOT REMOVE. end WaitForDetect_state_code
}

void SiguePelota3d::DirectMove_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin DirectMove_state_code
        float panPos, tiltPos;
        float panVel, tiltVel;
        
        if (true){
            directMoveDone = false;

        _NewBallDetector->get3DPosition(ball3d);
        _NewBallDetector->getImagePosition(ball2d);
            _Kinematics->getNeckAngles(
                    ball3d.X/ball3d.H, ball3d.Y/ball3d.H, ball3d.Z/ball3d.H, 
                    panPos, tiltPos
            );
            
            if (panPos != _panPos || tiltPos != !tiltPos){
                _panPos = panPos;
                _tiltPos = tiltPos;

                //PAN
                {
                    float value = abs(ball2d.x - center2d.x);
                    panVel = value/center2d.x;
                    if (panVel>0.5) panVel = 0.5; else if (panVel<0.1) panVel = 0.1;
        //            printf("diffX=%.3f (%.3f - %.3f) pan=%.3f\n", value, center2d.x, ball2d.x, panVel);
                }

                //TILT
                {
                    float value = abs(ball2d.y - center2d.y);
                    tiltVel = value/center2d.y;
                    if (tiltVel>0.5) tiltVel = 0.5; else if (tiltVel<0.1) tiltVel = 0.1;
        //            printf("diffY=%.3f (%.3f - %.3f) tilt=%.3f\n", value, center2d.y, ball2d.y, tiltVel);
                }

                _Head->setPanPos(panPos, 0.1);
                _Head->setTiltPos(tiltPos, 0.1);


                printf("DirectMove!\n panPos=%.3f panVel=%.3f\n tiltPos=%.3f tiltVel=%.3f\n", panPos, panVel, tiltPos, tiltVel);
            }
        }
//BUILDER COMMENT. DO NOT REMOVE. end DirectMove_state_code
}

bool SiguePelota3d::Initial2WaitForDetect_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2WaitForDetect_transition_code
    return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2WaitForDetect_transition_code
}

bool SiguePelota3d::WaitForDetect2RelativeMove0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin WaitForDetect2RelativeMove0_transition_code
    if ( _NewBallDetector->isDetected() ){
        _NewBallDetector->getImagePosition(ball2d);
        return true;
    }
    return false;
//BUILDER COMMENT. DO NOT REMOVE. end WaitForDetect2RelativeMove0_transition_code
}

bool SiguePelota3d::RelativeMove2WaitForDetect0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin RelativeMove2WaitForDetect0_transition_code
	return true;
//BUILDER COMMENT. DO NOT REMOVE. end RelativeMove2WaitForDetect0_transition_code
}

bool SiguePelota3d::WaitForDetect2DirectMove0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin WaitForDetect2DirectMove0_transition_code
    if ( _NewBallDetector->isDetected() ){

        return false;
    }
    return false;
//BUILDER COMMENT. DO NOT REMOVE. end WaitForDetect2DirectMove0_transition_code
}

bool SiguePelota3d::DirectMove2WaitForDetect0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin DirectMove2WaitForDetect0_transition_code
 //   if (true || getStopWatch() > 2*_Head->getFreqTime()){
 //       directMoveDone = true;
 //       return true;
 //   }
    return false;
//BUILDER COMMENT. DO NOT REMOVE. end DirectMove2WaitForDetect0_transition_code
}

void
SiguePelota3d::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2WaitForDetect_transition_code()) {
				state = WaitForDetect;
				resetStopWatch();
			}
		}

		break;
	case RelativeMove:

		if (isTime2Run()) {
			RelativeMove_state_code();

			if (RelativeMove2WaitForDetect0_transition_code()) {
				state = WaitForDetect;
			}
		}

		_Head->step();
		break;
	case WaitForDetect:
		_NewBallDetector->step();

		if (isTime2Run()) {
			WaitForDetect_state_code();

			if (WaitForDetect2DirectMove0_transition_code()) {
				state = DirectMove;
				resetStopWatch();
			}
			else if (WaitForDetect2RelativeMove0_transition_code()) {
				state = RelativeMove;
			}
		}

		break;
	case DirectMove:
		_NewBallDetector->step();
		_Kinematics->step();

		if (isTime2Run()) {
			DirectMove_state_code();

			if (DirectMove2WaitForDetect0_transition_code()) {
				state = WaitForDetect;
				resetStopWatch();
			}
		}

		_Head->step();
		break;
	default:
		cout << "[SiguePelota3d::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

