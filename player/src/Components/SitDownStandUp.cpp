#include "SitDownStandUp.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

SitDownStandUp::SitDownStandUp()
{
	_RFootLongPress = RFootLongPress::getInstance();
	_LFootLongPress = LFootLongPress::getInstance();
	_Body = Body::getInstance();
	_TestComp = TestComp::getInstance();
	_BumperDetector = BumperDetector::getInstance();
	state = Initial;
}

SitDownStandUp::~SitDownStandUp()
{

}

void SitDownStandUp::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void SitDownStandUp::Off_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Off_state_code
if (!_Body->isStopped()){
	_Body->setVel(0,0,0);
	_Body->selKick(Body::POSEINIT);
}

if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [Off]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end Off_state_code
}

void SitDownStandUp::On_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin On_state_code
//Call TestComp step()

if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [On]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end On_state_code
}

void SitDownStandUp::Down_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Down_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [Down]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end Down_state_code
}

void SitDownStandUp::StandUp_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin StandUp_state_code
// stand up (poseinit) order executed on the activation transition
// it must runed only once

if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [StandUp]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end StandUp_state_code
}

void SitDownStandUp::SitDown_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SitDown_state_code
// sit down order executed on the activation transition
// it must runed only once

if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [SitDown]\n";
}
//BUILDER COMMENT. DO NOT REMOVE. end SitDown_state_code
}

bool SitDownStandUp::Initial2Off_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2Off_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2Off_transition_code
}

bool SitDownStandUp::Off2On_RFootLong_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Off2On_RFootLong_transition_code
return _RFootLongPress->isActived();
//BUILDER COMMENT. DO NOT REMOVE. end Off2On_RFootLong_transition_code
}

bool SitDownStandUp::On2Off_LFootShort_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin On2Off_LFootShort_transition_code
bool Ll,Lr,Rl,Rr;
_BumperDetector->getValues(Ll,Lr,Rl,Rr);
return Lr;
//BUILDER COMMENT. DO NOT REMOVE. end On2Off_LFootShort_transition_code
}

bool SitDownStandUp::Down2StandUp_RFootLong_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Down2StandUp_RFootLong_transition_code
if ( _RFootLongPress->isActived() ){//@@@ || _DebugTestLongActivity->isActived()
	_Body->setStiffness(1);
	_Body->selKick(Body::POSEINIT); //this call changes Body's state machine to Moving
	return true;
}else{
	return false;
}
//BUILDER COMMENT. DO NOT REMOVE. end Down2StandUp_RFootLong_transition_code
}

bool SitDownStandUp::Off2SitDown_LFootLong_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Off2SitDown_LFootLong_transition_code
if ( _LFootLongPress->isActived() ){//@@@ || true
	_Body->selKick("sentarse");
	return true;
}else{
	return false;
}
//BUILDER COMMENT. DO NOT REMOVE. end Off2SitDown_LFootLong_transition_code
}

bool SitDownStandUp::SitDown2Down_KickMotionFinalized_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin SitDown2Down_KickMotionFinalized_transition_code
if ( _Body->isStopped() ){
	_Body->setStiffness(0);
	return true;
}else{
	return false;
}
//BUILDER COMMENT. DO NOT REMOVE. end SitDown2Down_KickMotionFinalized_transition_code
}

bool SitDownStandUp::StandUp2Off_KickMotionFinalized_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin StandUp2Off_KickMotionFinalized_transition_code
return _Body->isStopped();
//BUILDER COMMENT. DO NOT REMOVE. end StandUp2Off_KickMotionFinalized_transition_code
}

void
SitDownStandUp::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2Off_transition_code()) {
				state = Off;
			}
		}

		break;
	case Off:
		_RFootLongPress->step();
		_LFootLongPress->step();

		if (isTime2Run()) {
			Off_state_code();

			if (Off2On_RFootLong_transition_code()) {
				state = On;
			}
			else if (Off2SitDown_LFootLong_transition_code()) {
				state = SitDown;
			}
		}

		_Body->step();
		break;
	case On:
		_TestComp->step();
		_BumperDetector->step();

		if (isTime2Run()) {
			On_state_code();

			if (On2Off_LFootShort_transition_code()) {
				state = Off;
			}
		}

		break;
	case Down:
		_RFootLongPress->step();

		if (isTime2Run()) {
			Down_state_code();

			if (Down2StandUp_RFootLong_transition_code()) {
				state = StandUp;
			}
		}

		break;
	case StandUp:

		if (isTime2Run()) {
			StandUp_state_code();

			if (StandUp2Off_KickMotionFinalized_transition_code()) {
				state = Off;
			}
		}

		_Body->step();
		break;
	case SitDown:

		if (isTime2Run()) {
			SitDown_state_code();

			if (SitDown2Down_KickMotionFinalized_transition_code()) {
				state = Down;
			}
		}

		_Body->step();
		break;
	default:
		cout << "[SitDownStandUp::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

