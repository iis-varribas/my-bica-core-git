#include "Calibration.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

const string Calibration::UPPER_CAMERA_CONFIG_FILE =
		"/home/nao/bica/conf/ballPerception/cameraUpper.conf";
const string Calibration::LOWER_CAMERA_CONFIG_FILE =
		"/home/nao/bica/conf/ballPerception/cameraLower.conf";

using namespace std;

Calibration::Calibration()
{
	_Kinematics = Kinematics::getInstance();
	_WorldModel = WorldModel::getInstance();
	_CameraRGB = CameraRGB::getInstance();

	this->robotx = 0.0;
	this->roboty = 0.0;
	this->robott = 0.0;
	this->hasChanged = 0;

	this->imgSize = ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB * sizeof(char);


	this->mylines = new wmLine3D[CALIBRATION_MAX_LINES];

}

Calibration::~Calibration()
{
	delete this->mylines;
}

void 
Calibration::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker)
{
	Component::init(newName, parentBroker);

	this->setFreqTime(LONG_RATE);
}

void
Calibration::step()
{
	_CameraRGB->step();
	_Kinematics->step();

	if(isTime2Run())
	{
		_CameraRGB->getImageCopy(srcImg);
	}
}

bica::LinePtr
Calibration::createLine(HPoint2D p, HPoint2D q, bica::ColorType color, float width, float z)
{
	bica::Point3DPtr p1(new bica::Point3D);
	bica::Point3DPtr p2(new bica::Point3D);
	bica::LinePtr line(new bica::Line);

	p1->x = p.x;
	p1->y = p.y;
	p1->z = z;

	p2->x = q.x;
	p2->y = q.y;
	p2->z = z;

	line->p1 = p1;
	line->p2 = p2;
	line->width = width;
	line->color = color;
	line->filled = true;
	line->opacity = 255;
	line->accKey = "b";
	line->label = "";

	return line;
}

bica::ShapeList
Calibration::getGrDebugImg()
{
	shapeListImg.clear();

	bica::ImagePtr image(new bica::Image);
	image->pixelData.resize(imgSize);
	image->width = 320;
	image->height = 240;
	image->z = 0.0;

	image->color = bica::ORANGE;
	image->filled = false;
	image->opacity = 125;
	image->accKey = "b";
	image->label = "";

	memmove( &(image->pixelData[0]), (char *) srcImg, imgSize);//copy data to reply

	shapeListImg.push_back(image);

	HPoint2D p1, p2, p3, p4;
	int i, numl;

	/*Get the world model*/
	numl = _WorldModel->getCalibrationLines(this->mylines, CALIBRATION_MAX_LINES);

	/*Configure our camera*/
	_Kinematics->configureCamera(&(this->camera), robotx, roboty, robott);


	if(this->hasChanged) {
		this->camera.u0 = this->u0;
		this->camera.v0 = this->v0;
		this->camera.fdistx = this->fdistx;
		this->camera.fdisty = this->fdisty;
		this->camera.roll = this->roll;
		update_camera_matrix(&(this->camera));
	}


	/*Draw lines*/
	for(i=0;i<numl;i++) {
		/*Project 3D points in 2D*/
		_Kinematics->get2DPosition(&(this->camera), this->mylines[i].p1, p1);
		_Kinematics->get2DPosition(&(this->camera), this->mylines[i].p2, p2);
		_Kinematics->get2DPosition(&(this->camera), this->mylines[i].p3, p3);
		_Kinematics->get2DPosition(&(this->camera), this->mylines[i].p4, p4);

		if(_Kinematics->drawLine(&(this->camera), &p1, &p2))
		{
			shapeListImg.push_back(createLine(p1, p2, bica::RED, 1.0, 1.0));
		}

		if(_Kinematics->drawLine(&(this->camera), &p3, &p4))
		{
			shapeListImg.push_back(createLine(p3, p4, bica::RED, 1.0, 1.0));
		}

	}

	return shapeListImg;

}

bica::CalibrationParamsPtr
Calibration::getCalibrationParams(const Ice::Current& c)
{
	bica::CalibrationParamsPtr params(new bica::CalibrationParams);

	this->u0 = _Kinematics->getCamera()->u0;
	this->v0 = _Kinematics->getCamera()->v0;
	this->fdistx = _Kinematics->getCamera()->fdistx;
	this->fdisty = _Kinematics->getCamera()->fdisty;
	this->roll = _Kinematics->getCamera()->roll;

	params->robotx = robotx;
	params->roboty = roboty;
	params->robott = robott;
	params->u0 = u0;
	params->v0 = v0;
	params->fdistx = fdistx;
	params->fdisty = fdisty;
	params->roll = roll;

	return params;
}

void
Calibration::saveCalibrationParams (const Ice::Current& c)
{

	_Kinematics->updateIntrinsicParams(this->u0, this->v0, this->fdistx, this->fdisty, this->roll);

	_Kinematics->saveCameraIntrinsicParams();
}

void
Calibration::setCalibrationParams (const CalibrationParamsPtr& newParams, const Ice::Current& c)
{
	this->hasChanged = 1;

	this->robotx = newParams->robotx;
	this->roboty = newParams->roboty;
	this->robott = newParams->robott;
	this->u0 = newParams->u0;
	this->v0 = newParams->v0;
	this->fdistx = newParams->fdistx;
	this->fdisty = newParams->fdisty;
	this->roll = newParams->roll;
}

void
Calibration::setCameraParams (const Ice::Current& c)
{
	AL::ALVideoDeviceProxy *pcamera;
	int cam;
	string fLEMname;

	try {
		pcamera = new ALVideoDeviceProxy(getParentBroker());
		cam = pcamera->getParam(kCameraSelectID);
		fLEMname = pcamera->subscribe(string("Calib_GVM"),
				kQVGA, //320 x 240
				kRGBColorSpace,
				30);
	} catch (ALError& e) {
		cerr << "[Calibration::setCameraParams()] " << e.toString() << endl;
	}

	// Set camera to auto
	try {
			pcamera->setParam(kCameraAutoGainID, 1);
			pcamera->setParam(kCameraAutoExpositionID, 1);
			pcamera->setParam(kCameraAutoWhiteBalanceID, 1);
		} catch (ALError& e) {
			cerr << "Could not set cam param: " << e.toString() << endl;
		}

	usleep(1000000); //wait one second

	// Set camera to auto
	try {
			pcamera->setParam(kCameraAutoGainID, 0);
			pcamera->setParam(kCameraAutoExpositionID, 0);
			pcamera->setParam(kCameraAutoWhiteBalanceID, 0);
		} catch (ALError& e) {
			cerr << "Could not set cam param: " << e.toString() << endl;
		}
	Dictionary cameraconf;

	cameraconf.setValueInt("kCameraContrastID", pcamera->getParam(kCameraBrightnessID));
	cameraconf.setValueInt("kCameraContrastID", pcamera->getParam(kCameraContrastID));
	cameraconf.setValueInt("kCameraSaturationID", pcamera->getParam(kCameraSaturationID));
	cameraconf.setValueInt("kCameraHueID", pcamera->getParam(kCameraHueID));
	cameraconf.setValueInt("kCameraRedChromaID", pcamera->getParam(kCameraRedChromaID));
	cameraconf.setValueInt("kCameraBlueChromaID", pcamera->getParam(kCameraBlueChromaID));
	cameraconf.setValueInt("kCameraGainID", pcamera->getParam(kCameraGainID));
	cameraconf.setValueInt("kCameraLensXID", pcamera->getParam(kCameraLensXID));
	cameraconf.setValueInt("kCameraLensYID", pcamera->getParam(kCameraLensYID));

	if(cam == ImageInput::UPPER_CAMERA)
		cameraconf.saveToFile(UPPER_CAMERA_CONFIG_FILE.c_str());
	else
		cameraconf.saveToFile(LOWER_CAMERA_CONFIG_FILE.c_str());

}

