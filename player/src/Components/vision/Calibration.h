#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "Singleton.h"
#include "Kinematics.h"
#include "CameraRGB.h"
#include "ImageInput.h"
#include "WorldModel.h"
#include <string>

#include <Ice/Ice.h>
#include <image.h>
#include <debug.h>
#include "Debug.h"
#include "componentsI.h"

using namespace std;
using namespace bica;

class Calibration: public Component, public Singleton<Calibration>, public DebugIImg, public CalibrationProvider
{
public:
	Calibration();
	~Calibration();

	void step();

	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);

	virtual CalibrationParamsPtr getCalibrationParams(const Ice::Current& c);
	virtual void setCalibrationParams (const CalibrationParamsPtr& newParams, const Ice::Current& c);
	virtual void saveCalibrationParams (const Ice::Current& c);
	virtual void setCameraParams (const Ice::Current& c);

	bica::ShapeList getGrDebugImg();

private:

	char srcImg[ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB];
	int imgSize;

	static const int CALIBRATION_MAX_LINES = 50;
	static const string UPPER_CAMERA_CONFIG_FILE;
	static const string LOWER_CAMERA_CONFIG_FILE;

	bica::LinePtr createLine(HPoint2D p, HPoint2D q, bica::ColorType color, float width, float z);


	Kinematics *_Kinematics;
	CameraRGB *_CameraRGB;
	WorldModel *_WorldModel;

	TPinHoleCamera camera;
	wmLine3D * mylines;

	float robotx;
	float roboty;
	float robott;
	float u0;
	float v0;
	float fdistx;
	float fdisty;
	float roll;
	int hasChanged;
};

#endif
