#ifndef CAMERARGB_H
#define CAMERARGB_H

#include <iostream>
#include <cmath>
#include <pthread.h>
#include "Component.h"
#include "alvision/alvisiondefinitions.h"
#include "alproxies/alvideodeviceproxy.h"
#include "alvision/alimage.h"
#include "ImageInput.h"
#include "Singleton.h"
#include "Kinematics.h"

#include <Ice/Ice.h>
#include <image.h>

using namespace std;
using namespace bica;



class CameraRGB : public Component, public Singleton<CameraRGB>
{
public:

	CameraRGB();
	~CameraRGB();

	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);
	void step();

	void getImageCopy(char *img);

private:

	void initCamera();
	void loadCameraParams(const string upper_file, const string lower_file);
	bool newImage();
	bool setCamParam(const int param, const int value);
	int getCamParam(const int param);
	void setCam(const int cam);

	static const string UPPER_CAMERA_CONFIG_FILE;
	static const string LOWER_CAMERA_CONFIG_FILE;

	int cam;
	bool camera_initialized;
	string fLEMname;
	pthread_mutex_t mutex;
	Dictionary	camConfUpper, camConfLower;

	bool imageTaken;

	AL::ALVideoDeviceProxy *pcamera;
	bica::ImageDescriptionPtr imgDescription;

	Kinematics	* _Kinematics;

	long long lastTimeStamp;
	long wtime;

	//capture thread
	pthread_t capture_thread;

	bool newImageTaken;
	char imgBuff[ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB];
	long image_ts;
	long imagereq_ts;

protected:
	static void * EntryPoint(void*);
	void Capture();

};

#endif
