#include "BasicDetector.h"
#include "Kinematics.h"


BasicDetector::BasicDetector()
{

}

BasicDetector::~BasicDetector()
{
}

void
BasicDetector::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker)
{
	Component::init(newName, parentBroker);

	_CameraRGB = CameraRGB::getInstance();
	kinematics =  Kinematics::getInstance();

	this->setFreqTime(SHORT_RATE);

}

void
BasicDetector::step()
{
	_CameraRGB->step();
	kinematics->step();

	if(isTime2Run())
	{
		this->detect();
	}

}

void
BasicDetector::detect()
{
	_CameraRGB->getImageCopy(srcImg);

	int r,g,b;
	int x,y;

	x = ImageInput::IMG_WIDTH/2;
	y = ImageInput::IMG_HEIGHT/2;

	r = (int)srcImg[(ImageInput::IMG_WIDTH * y + x)*3] + 128;
	g = (int)srcImg[(ImageInput::IMG_WIDTH * y + x)*3 + 1] +128;
	b = (int)srcImg[(ImageInput::IMG_WIDTH * y + x)*3 + 2] + 128;

	cerr<<"["<<r<<", "<<g<<", "<<b<<"]"<<endl;

}

