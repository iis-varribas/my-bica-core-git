#ifndef BASICDETECTOR_H_
#define BASICDETECTOR_H_

#include "Detector.h"
#include "CameraRGB.h"
#include "Debug.h"
#include "ImageInput.h"

class CameraRGB;

class BasicDetector: public Component, public Singleton<BasicDetector>
{
public:
	BasicDetector();
	~BasicDetector();

	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);
	void step();
private:

	void detect();

	CameraRGB		*_CameraRGB;
	Kinematics 		*kinematics;

	char srcImg[ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB];
};

#endif /* BASICDETECTOR_H_ */
