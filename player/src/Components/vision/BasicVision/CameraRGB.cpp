#include "CameraRGB.h"

const string CameraRGB::UPPER_CAMERA_CONFIG_FILE =
		"/home/nao/bica/conf/ballCameraRGB/cameraUpper.conf";
const string CameraRGB::LOWER_CAMERA_CONFIG_FILE =
		"/home/nao/bica/conf/ballCameraRGB/cameraLower.conf";


CameraRGB::CameraRGB() {

	_Kinematics = Kinematics::getInstance();

	camera_initialized = false;

	camConfUpper.createDictionary();
	camConfLower.createDictionary();

	pthread_mutex_init(&mutex, NULL);

	setFreqTime(ImageInput::CAPTURE_RATE);

	newImageTaken = false;

	struct timeval current;
	long current_m;

	gettimeofday(&current, NULL);
	current_m = current.tv_sec * 1000000 + current.tv_usec;

	lastTimeStamp = current_m;
	wtime=0;

	struct timeval s1;
	gettimeofday(&s1, NULL);
	imagereq_ts = s1.tv_sec * 1000000 + s1.tv_usec;

}

CameraRGB::~CameraRGB() {

	try {
		pcamera->unsubscribe(fLEMname);
	} catch (ALError& e) {
		cerr << "Unable to unsubscribe Lem: " << e.toString() << endl;
	}

}

void
CameraRGB::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker)
{
	Component::init(newName, parentBroker);

	initCamera();

	int rc;

	rc =  pthread_create(&capture_thread, NULL, CameraRGB::EntryPoint, this);

    if (rc){
       printf("ERROR; return code from pthread_create() is %d\n", rc);
       exit(-1);
    }

}

/*Camera parameters*/
void
CameraRGB::loadCameraParams(const string upper_file, const string lower_file)
{
	Dictionary *conf;
	ostringstream s;

	camConfUpper.loadFromFile(upper_file.c_str());
	camConfLower.loadFromFile(lower_file.c_str());

	int newkCameraBrightnessID, newkCameraContrastID, newkCameraSaturationID,
	newkCameraHueID, newkCameraRedChromaID, newkCameraBlueChromaID,
	newkCameraGainID, newkCameraLensXID, newkCameraLensYID,
	newkCameraAutoGainID, newkCameraAutoExpositionID,
	newkCameraAutoWhiteBalanceID;

	for (int cameraID = 1; cameraID >= 0; cameraID--) {

		if (cameraID == ImageInput::LOWER_CAMERA)
			conf = &camConfLower;
		else if (cameraID == ImageInput::UPPER_CAMERA)
			conf = &camConfUpper;
		else
			s << "[CameraRGB::loadCameraParams()] Bad camera identifier (" << cameraID << endl;

		// Read the camera parameters
		if ((conf->getValueInt("kCameraBrightnessID", &newkCameraBrightnessID)) &&
				(conf->getValueInt("kCameraContrastID", &newkCameraContrastID)) &&
				(conf->getValueInt("kCameraSaturationID", &newkCameraSaturationID)) &&
				(conf->getValueInt("kCameraHueID", &newkCameraHueID)) &&
				(conf->getValueInt("kCameraRedChromaID", &newkCameraRedChromaID)) &&
				(conf->getValueInt("kCameraBlueChromaID", &newkCameraBlueChromaID)) &&
				(conf->getValueInt("kCameraGainID", &newkCameraGainID)) &&
				(conf->getValueInt("kCameraLensXID", &newkCameraLensXID)) &&
				(conf->getValueInt("kCameraLensYID", &newkCameraLensYID)) &&
				(conf->getValueInt("kCameraAutoGainID",	&newkCameraAutoGainID)) &&
				(conf->getValueInt("kCameraAutoExpositionID", &newkCameraAutoExpositionID)) &&
				(conf->getValueInt("kCameraAutoWhiteBalanceID", &newkCameraAutoWhiteBalanceID)))
			s << "[CameraRGB] Loaded camera parameters for camera " << cameraID << endl;
		else
			s << "[CameraRGB] Problem loading parameters for camera " << cameraID << endl;

		// Set the camera parameters
		try {
			pcamera->setParam(kCameraSelectID, cameraID);
			usleep(250000);

			pcamera->setParam(kCameraBrightnessID, newkCameraBrightnessID);
			pcamera->setParam(kCameraContrastID, newkCameraContrastID);
			pcamera->setParam(kCameraSaturationID, newkCameraSaturationID);
			pcamera->setParam(kCameraHueID, newkCameraHueID);
			pcamera->setParam(kCameraRedChromaID, newkCameraRedChromaID);
			pcamera->setParam(kCameraBlueChromaID, newkCameraBlueChromaID);
			pcamera->setParam(kCameraGainID, newkCameraGainID);
			pcamera->setParam(kCameraLensXID, newkCameraLensXID);
			pcamera->setParam(kCameraLensYID, newkCameraLensYID);

			// Automatic values
			pcamera->setParam(kCameraAutoGainID, newkCameraAutoGainID);
			pcamera->setParam(kCameraAutoExpositionID, newkCameraAutoExpositionID);
			pcamera->setParam(kCameraAutoWhiteBalanceID, newkCameraAutoWhiteBalanceID);
		} catch (ALError& e) {
			s << "Could not set cam param: " << e.toString() << endl;
		}
	}
	writeLog(s.str());
}

void
CameraRGB::initCamera(void)
{
	try {
		pcamera = new ALVideoDeviceProxy(getParentBroker());
		cam = pcamera->getParam(kCameraSelectID);
		fLEMname = pcamera->subscribe(string("BasicRGB_GVM"),
				kQVGA, //320 x 240
				kRGBColorSpace,
				30);
	} catch (ALError& e) {
		cerr << "[CameraRGB::initCamera()] " << e.toString() << endl;
	}


	camera_initialized = true;

	for (int i = 0; i < 2; i++)
		loadCameraParams(UPPER_CAMERA_CONFIG_FILE, LOWER_CAMERA_CONFIG_FILE);

	setCam(ImageInput::LOWER_CAMERA);
}


void *CameraRGB::EntryPoint(void * pthis)
{
	CameraRGB *pt = CameraRGB::getInstance();
    pt->Capture();
}

void CameraRGB::Capture()
{
	struct timeval s1;
	long s,e;


	while(true)
	{
		gettimeofday(&s1, NULL);
		s = s1.tv_sec * 1000000 + s1.tv_usec;

		while((s-imagereq_ts)>1000000)
		{
			newImageTaken = false;
			usleep(10000); //don't waste cpu if not active
			gettimeofday(&s1, NULL);
			s = s1.tv_sec * 1000000 + s1.tv_usec;
		}

	#ifndef PLAYER_IS_REMOTE		//Get new image (local mode)
		//cerr<<"+"<<endl;
		ALImage* newImage = NULL;

		/*gettimeofday(&s1, NULL);
		s = s1.tv_sec * 1000000 + s1.tv_usec;
		cerr<<"cs ["<<s<<"]"<<endl;
		*/
		try {

#ifdef WEBOTS
			//cerr<<"*";
			usleep(25000); //webots does not block for 25 ms
#endif
			newImage = (ALImage*) (pcamera->getImageLocal(fLEMname));
			_Kinematics->forceStep();
		} catch( ALError& e) {
			cerr << "[CameraRGB::getImageLocal()] Error: " << e.toString() << endl;
		}

		/*gettimeofday(&s1, NULL);
		e = s1.tv_sec * 1000000 + s1.tv_usec;
		cerr<<"ce ["<<s<<"] "<<e-s<<endl;
		 */
		pthread_mutex_lock(&mutex);
		memcpy(imgBuff, (char*)(newImage->getData()), ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB);

		pthread_mutex_unlock(&mutex);

		gettimeofday(&s1, NULL);
		newImageTaken = true;
		image_ts = s1.tv_sec * 1000000 + s1.tv_usec;

		try {
			pcamera->releaseImage(fLEMname);
		}catch (ALError& e) {
			cerr << "Error in releasing image: " << e.toString() << endl;
		}



	#else							//Get new image (remote mode)
		//cerr<<"-"<<endl;
		ALValue newImage;

		try {
#ifdef WEBOTS
			//cerr<<"*";
			usleep(25000); //webots does not block for 25 ms
#endif
			newImage = pcamera->getImageRemote(fLEMname);
			_Kinematics->forceStep();
		} catch (ALError& e) {
			cerr << "[CameraRGB::getImageRemote()] Error: " << e.toString() << endl;
		}
//		ostringstream s;
//		s<<"IC\t"<<getCurrentTime()<<"\t0.0"<<endl;
//		s<<"IC\t"<<getCurrentTime()<<"\t1.0"<<endl;
//		s<<"IC\t"<<getCurrentTime()<<"\t0.0"<<endl;
//		writeLog(s.str());


		pthread_mutex_lock(&mutex);
		memcpy(imgBuff, (char*) static_cast<const char*> (newImage[6].GetBinary()), ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB);
		pthread_mutex_unlock(&mutex);

		gettimeofday(&s1, NULL);
		newImageTaken = true;
		image_ts = s1.tv_sec * 1000000 + s1.tv_usec;

	#endif

	}

}


bool
CameraRGB::newImage()
{
	struct timeval s1;

	gettimeofday(&s1, NULL);
	imagereq_ts = s1.tv_sec * 1000000 + s1.tv_usec;

	if(newImageTaken)
	{
		long end, start;


		newImageTaken = false;

		wtime = (end-start);
		return true;
	}else
	{
		return false;
	}

}

void
CameraRGB::step(void)
{

	if (isTime2Run())
	{
		/*Get a new image*/
		this->newImage();
	}


}

void
CameraRGB::getImageCopy(char *img)
{
	pthread_mutex_lock(&mutex);
	memcpy(img, imgBuff, ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * ImageInput::IMG_CHANNELS_RGB);
	pthread_mutex_unlock(&mutex);
}

bool
CameraRGB::setCamParam(const int param, const int value)
{
	ostringstream s;
	try {
		pcamera->setParam(param, value);
		return true;
	} catch (ALError& e) {
		s << "Could not set cam param: " << e.toString() << endl;
		return false;
	}
}

int
CameraRGB::getCamParam(const int param)
{
	ostringstream s;
	try {
		return pcamera->getParam(param);
	} catch (ALError& e) {
		s << "Could not set cam param: " << e.toString() << endl;
		return -1;
	}
}

void
CameraRGB::setCam(const int cam) {
	if (!camera_initialized)
		initCamera();

	try {
		pcamera->setParam(kCameraSelectID, cam);
		this->cam = cam;
	} catch (ALError& e) {
		cerr << "Unable to change camera: " << e.toString() << endl;
	}
}

