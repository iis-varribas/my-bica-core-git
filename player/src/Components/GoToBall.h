#ifndef GoToBall_H
#define GoToBall_H

#include "Component.h"
#include "NewBallDetector.h"
#include "Kinematics.h"
#include "Body.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "head/head/Head.h"
#include <alcore/alptr.h>
#include <alcommon/alproxy.h>
#include "alcommon/albroker.h"
#include "alproxies/almotionproxy.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class GoToBall : public Component, public Singleton<GoToBall>
{
public:

	GoToBall();
	~GoToBall();

	void step();
private:

	static const int Initial	= 0;
	static const int Moving	= 1;
	static const int Done	= 2;
	static const int WaitForBall	= 3;
	int state;

	NewBallDetector *_NewBallDetector;
	Head *_Head;
	Kinematics *_Kinematics;
	Body *_Body;

	void Initial_state_code(void);
	void Moving_state_code(void);
	void Done_state_code(void);
	void WaitForBall_state_code(void);
	bool Moving2Done_BallIsNear_transition_code(void);
	bool Done2Moving_BallIsFar_transition_code(void);
	bool WaitForBall2Moving_BallDetected_transition_code(void);
	bool Initial2WaitForBall0_transition_code(void);
	bool Moving2WaitForBall_NotDetected_transition_code(void);
	bool Done2WaitForBall_BallLost_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
        //Head parameters (in radians)
        static const float TARGET_PAN  = -0.04f; //Initial: 0.0 Center: 0.0 Left: 1.2 Right: -1.2
        static const float TARGET_TILT = 0.50f; //Initial: -0.30 Down: 0.51 Up: -0.67
        static const float pan_error  = 0.01;
        static const float tilt_error = 0.01;
    
        //Ball parameters
        static const float BALL_X = 160.0;
        static const float BALL_Y = 10.0;
        static const float x_error = 0.0;
        static const float y_error = 5.0;
            
        void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);

	bool moveIsDone();
private:
        HPoint2D ball_p2d;
        HPoint3D ball_p3d;
        float pan, tilt;
        
        
        bool v_bool;
        AL::ALPtr<AL::ALMotionProxy> pmotion;
public:
        float getValue(string str);
        float getValueAsMin(float in_value, float out_default, float limits[], float values[], int N);

private:
	//DAVID
	TKinematics myKinematics;

private:
	int INT_forward;
	int INT_lateral;
	int INT_angular; 

	float FORWARD;
	float LATERAL;
	float ANGULAR;            
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

