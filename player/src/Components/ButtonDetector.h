/*
 * Name: ButtonDetector.h
 * @Author: Alejandro Aporta (a.aporta@alumnos.urjc.es)
 * 
 * 
 * Description: Class to detect other robots
 *
 * Created on: 04/11/2011
 *
 * Copyright (C) Universidad Rey Juan Carlos
 * All Rights Reserved.
 *
 */

#ifndef ButtonDetector_H_
#define ButtonDetector_H_

#include "Component.h"
#include "Singleton.h"

#include "Matrix.h"



class ButtonDetector: public Component, public Singleton<ButtonDetector>{
public:
	ButtonDetector();
	~ButtonDetector();

	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);
	void step();

	//Do NOT use these methods for reading buttons
	void RightBumperPressed();
	void LeftBumperPressed();
	void ChestButtonPressed();

	//Use this method to read button
	void getValues(bool &rleft, bool &rright, bool &rchest);

private:

	bool left, right, chest;
	int cont;
};

#endif /* ButtonDetector_H_ */

