/*
 * Name: USDetector.h
 * @Author: Alejandro Aporta (a.aporta@alumnos.urjc.es)
 * 
 * 
 * Description: Class to detect other robots
 *
 * Created on: 04/11/2011
 *
 * Copyright (C) Universidad Rey Juan Carlos
 * All Rights Reserved.
 *
 */

#ifndef USDetector_H_
#define USDetector_H_

#include "Component.h"
#include "Singleton.h"
#include "Body.h"
#include "Debug.h"
#include "progeo.h"


#include "Matrix.h"



class USDetector: public Component, public DebugIRel, public Singleton<USDetector>{
public:
	USDetector();
	~USDetector();

	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);
	void step();
	void dowork(void);

	void getValues(HPoint3D &rleft, HPoint3D &rright);

	bica::ShapeList getGrDebugRel();
private:

	static const int INITIAL_STATE	= 0;
	static const int TEST_STATE = 1;

	double sonar_value_right;
	double sonar_value_left;

	Body			*body;

	AL::ALPtr<AL::ALMemoryProxy> pmemory;
	AL::ALPtr<AL::ALMotionProxy> pmotion;
	
	HPoint3D left, right;

	vector<float> torsoRT;
};

#endif /* USDetector_H_ */

