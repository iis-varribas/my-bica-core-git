#include "GoalDetector.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

GoalDetector::GoalDetector()
{
	_CameraRGB = CameraRGB::getInstance();
	_Kinematics = Kinematics::getInstance();
	state = Initial;
        
        //VALORES INICIALES DE LOS POSTES (WEBOTS)
        poste1.x=3; //metros
        poste1.y=0.7;
        
        poste2.x=-3; //metros
        poste2.y=0.7;
        
        poste3.x=-3; //metros
        poste3.y=-0.7;
        
        poste4.x=3; //metros
        poste4.y=-0.7;
        
        image::filterGoal::aPolares(&poste1);
        image::filterGoal::aPolares(&poste2);
        image::filterGoal::aPolares(&poste3);
        image::filterGoal::aPolares(&poste4);
        
                
        this->setFreqTime(_CameraRGB->getFreqTime());
//        setFreqTime(ImageInput::CAPTURE_RATE);
}

GoalDetector::~GoalDetector()
{

}

void GoalDetector::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code    
//#ifdef WEBOTS
    
    //PARA PORTERIA AMARILLA
    /*
    hsv_min.h = 44;
    hsv_min.s = 197;
    hsv_min.v = 100;

    hsv_max.h = 45;
    hsv_max.s = 203;
    hsv_max.v = 114;
    */
    //VALIDOS
    hsv_min.h = 21;
    hsv_min.s = 164;
    hsv_min.v = 76;

    hsv_max.h = 78;
    hsv_max.s = 223;
    hsv_max.v = 121;
    
    /*
#else

    hsv_min.h = 0;
    hsv_min.s = 61;
    hsv_min.v = 178;

    hsv_max.h = 176;
    hsv_max.s = 144;
    hsv_max.v = 255;
     */
//#endif
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void GoalDetector::NotDetected_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin NotDetected_state_code
	//startDebugInfo();

	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);
        
        //Busca un recuadro con el mayor numero de pixels del color indicado
        // en la funcion de test posible
        
        //PARA LA VARIANZA MEDIR 20 VECES
        for(int i=0;i<20;i++){
            
	image::filterGoal::searchBestGoal(
                &coord1,
                &coord2,
		&imagenRGB, 
		funcionTest
	);
        
        HPoint3D p3d1;
	memset(&p3d1, 0, sizeof(HPoint3D));
        HPoint3D p3d2;
	memset(&p3d2, 0, sizeof(HPoint3D));
        
        
	get3DPosition(&coord1,p3d1);
        get3DPosition(&coord2,p3d2);
        
        image::filterGoal::aPolares(&coord1);
        image::filterGoal::aPolares(&coord2);
        
	printf("3D x:%.2f y:%.2f z:%.2f\n", p3d1.X/p3d1.H, p3d1.Y/p3d1.H, p3d1.Z/p3d1.H);
        coord1.x=p3d1.X/p3d1.H;
        coord1.y=p3d1.Y/p3d1.H;
        printf("3D2 x2:%.2f y2:%.2f z2:%.2f\n", p3d2.X/p3d2.H, p3d2.Y/p3d2.H, p3d2.Z/p3d1.H);
        coord2.x=p3d2.X/p3d2.H;
        coord2.y=p3d2.Y/p3d2.H;
        
        if(coord1.r>=0){
            varR=varR+coord1.r;
            varT=varT+coord1.theta;
        }
        }
        //Calculo varianza
        varR= varR/20;
        varT=varT/20;
        varR= (poste1.r)-varR;
        varT= (poste1.theta)-varT;
        varR= varR*varR;
        varT= varT*varT;
        printf("\nFIN\n\n\nvarR:%lf varT:%lf\n\n",varR,varT);
                
	//endDebugInfo();
//BUILDER COMMENT. DO NOT REMOVE. end NotDetected_state_code
}

void GoalDetector::Detected_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Detected_state_code
	//startDebugInfo();
        /*
#ifdef VERBOSE
	HPoint3D p3d;
	memset(&p3d, 0, sizeof(HPoint3D));
	this->get3DPosition(p3d);
	printf("3D x:%.2f y:%.2f z:%.2f\n", p3d.X/p3d.H, p3d.Y/p3d.H, p3d.Z/p3d.H);
#endif
         */
        
	//endDebugInfo();
//BUILDER COMMENT. DO NOT REMOVE. end Detected_state_code
}

bool GoalDetector::Initial2NotDetected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2NotDetected0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2NotDetected0_transition_code
}

bool GoalDetector::NotDetected2Detected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin NotDetected2Detected0_transition_code
    //MIO CAMBIAR
    return false;
    //FIN MIO
//BUILDER COMMENT. DO NOT REMOVE. end NotDetected2Detected0_transition_code
}

bool GoalDetector::Detected2NotDetected0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Detected2NotDetected0_transition_code
    //CAMBIAR
    return false;
//BUILDER COMMENT. DO NOT REMOVE. end Detected2NotDetected0_transition_code
}

void
GoalDetector::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2NotDetected0_transition_code()) {
				state = NotDetected;
			}
		}

		break;
	case NotDetected:
		_CameraRGB->step();

		if (isTime2Run()) {
			NotDetected_state_code();

			if (NotDetected2Detected0_transition_code()) {
				state = Detected;
			}
		}

		break;
	case Detected:
		_CameraRGB->step();
		_Kinematics->step();

		if (isTime2Run()) {
			Detected_state_code();

			if (Detected2NotDetected0_transition_code()) {
				state = NotDetected;
			}
		}

		break;
	default:
		cout << "[GoalDetector::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin

image::hsv_color GoalDetector::hsv_min;
image::hsv_color GoalDetector::hsv_max;

bool
GoalDetector::funcionTest(const image::rgb_color *pixel){
            
	image::hsv_color hsv;
        
	image::rgb2hsv(*pixel, hsv);
	if (	   (hsv.h >= hsv_min.h && hsv.h <= hsv_max.h)
		&& (hsv.s >= hsv_min.s && hsv.s <= hsv_max.s)
		&& (hsv.v >= hsv_min.v && hsv.v <= hsv_max.v)
	){
		return true;
	}else{
		return false;
	}
}
void 
GoalDetector::getImagePosition(image::filterGoal::tGoalCoord *coord,HPoint2D &p2d){
	p2d.x = coord->x;
	p2d.y = coord->y;
	p2d.h = 1.0;
}

void 
GoalDetector::get2DPosition(image::filterGoal::tGoalCoord *coord,HPoint2D &p2d){
	this->getImagePosition(coord,p2d);
	_Kinematics->pixel2Optical(_Kinematics->getCamera(), &p2d);
}

void 
GoalDetector::get3DPosition(image::filterGoal::tGoalCoord *coord,HPoint3D &p3d){
	HPoint2D p2d;

	this->getImagePosition(coord,p2d);
	_Kinematics->get3DPositionZ(p3d, p2d, 0);
	if (p3d.H != 1.0) {
		p3d.X /= p3d.H;
		p3d.Y /= p3d.H;
		p3d.Z /= p3d.H;
		p3d.H = 1.0;
	}
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

