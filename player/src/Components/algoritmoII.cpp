#include "algoritmoII.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


namespace image{
namespace filter{

void umbralizar(ImagenRGB *imagen, bool(*pfuncionTest)(const rgb_color* pixel), rgb_color positive, rgb_color negative){
	for (int h=0; h<imagen->HEIGHT; h++)
		for (int w=0; w<imagen->WIDTH; w++)
			if ( pfuncionTest(&(imagen->pixel[h][w])) )
				imagen->pixel[h][w]=positive;
			else
				imagen->pixel[h][w]=negative;
}

void luckyCenter(tParticula *particula, 
	const ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int w_min, unsigned int h_min,
	unsigned int w_max, unsigned int h_max,
	bool(*pfuncionTest)(const rgb_color* pixel)
){
        //Calcula el centro del objeto
    
	unsigned int x_M = 0;
	unsigned int y_M = 0;
	unsigned int x_m = imagen->WIDTH;
	unsigned int y_m = imagen->HEIGHT;
	unsigned long X=0, Y=0, N=0;

	memset(particula, 0, sizeof(tParticula));

	#ifdef ALGORITMOII_H_VERBOSE
		printf("luckyCenter() init\n");
	#endif
        //Recorre el area seleccionada
        //Calcula la media de las X correspondientes a pixeles
        // validos y de las Y por separado
	for (int h=h_min; h<h_max; h+=h_step){
		for (int w=w_min; w<w_max; w+=w_step){
			if ( pfuncionTest(&(imagen->pixel[h][w])) ){
				X += w;
				Y += h;
				N++;
				if (w<x_m) x_m = w;
				if (h<y_m) y_m = h;

				if (w>x_M) x_M = w;
				if (h>y_M) y_M = h;
			}
		}
	}
	if (N>0){
		particula->peso = N;

		particula->px = X/N;
		particula->py = Y/N;

		particula->left  = particula->px - x_m;
		particula->right = x_M - particula->px;

		particula->up   = particula->py - y_m;
		particula->down = y_M - particula->py;
	}
}

void paintNet(ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int w_min, unsigned int h_min,
	unsigned int w_max, unsigned int h_max,
	rgb_color color
){
	for (int h=h_min; h<h_max && h<imagen->HEIGHT; h+=h_step)
		for (int w=w_min; w<w_max && w<imagen->WIDTH; w+=w_step)
			imagen->pixel[h][w] = color;
}


void searchBest(tParticula *particula, 
	const ImagenRGB *imagen,
	unsigned int w_step, unsigned int h_step,
	unsigned int c_ancho, unsigned int c_alto,
	bool(*pfuncionTest)(const rgb_color* pixel),
	unsigned int grow_step, unsigned int p_pn_relation
){
	unsigned int w_len, h_len;
	tParticula p_new, p_selected;

	#ifdef ALGORITMOII_H_VERBOSE
		printf("searchBest() init\n");
	#endif

	memset(particula, 0, sizeof(tParticula));
	p_selected.up=(c_alto/2);
	p_selected.down=(c_alto-p_selected.up);
	p_selected.left=(c_ancho/2);
	p_selected.right=(c_ancho-p_selected.left);
	p_selected.peso=0;

	for (int h=0; h<imagen->HEIGHT; h+=h_step){
		for (int w=0; w<imagen->WIDTH; w+=w_step){
			if ( pfuncionTest(&(imagen->pixel[h][w])) ){
				p_new = p_selected;
				p_new.px = w;
				p_new.py = h;

				if (grow_step == 0){
					darPeso(&p_new, imagen, pfuncionTest);                                        
                                }else{
                                    
					growParticle(&p_new, imagen, grow_step, p_pn_relation, pfuncionTest);
					shrinkParticle(&p_new, imagen, grow_step, p_pn_relation, pfuncionTest);
				}
//printf("P [%d, %d] %d\n", p_new.px, p_new.py, p_new.peso);
				if (p_new.peso > p_selected.peso){

					p_selected = p_new;
					w_len = p_new.left + p_new.right;
					h_len = p_new.down + p_new.up;
					if (p_new.peso == w_len*h_len){
						#ifdef ALGORITMOII_H_VERBOSE
							printf("searchBest() Break!\n");
						#endif
						*particula = p_selected;
						return;
					}
				}
			}
		}
	}
	if (p_selected.peso > 0) *particula = p_selected;	
}


bool clipParticle(tParticula *particula, unsigned int x, unsigned int y, unsigned int X, unsigned int Y){
        //Ajusta la particula (recuadro), para que no supere los limites de la
        //pantalla.
    
	bool bounded = false;
	#ifdef ALGORITMOII_H_DEBUG
		printf("clip(before) %d %d %d %d\n", particula->left,particula->right,particula->up,particula->down);
	#endif
        //Si la mitad izquierda es mayor que la distancia entre el margen izq
        // y la posicion actual, el margen izquierdo de la particula sera el de
        // la pantalla
	if (particula->left > particula->px - x){
		particula->left = x;
		bounded = true;
	}

	if (particula->up > particula->py - y){
		particula->up = y;
		bounded = true;
	}

	if (particula->px + particula->right > X){
		particula->right = X - particula->px;
		bounded = true;
	}

	if (particula->py + particula->down > Y){
		particula->down = Y - particula->py;
		bounded = true;
	}

	#ifdef ALGORITMOII_H_DEBUG
		printf("clip(after) %d %d %d %d\n", particula->left,particula->right,particula->up,particula->down);
	#endif

	return bounded;
}


bool darPeso(tParticula *particula, const ImagenRGB *imagen, bool(*pfuncionTest)(const rgb_color* pixel)){
	unsigned int x,y;
	unsigned int peso=0;
	bool bounded = false;

        //Ajusta la particula (recuadro), para que no supere los limites de la
        //imagen
	bounded = clipParticle(particula, 0,0, imagen->WIDTH, imagen->HEIGHT);

        //Recorre la particula (recuadro) y comprueba el numero de pixels que
        //son del color indicado en la funcion Test
	for (y = particula->py - particula->up; y < particula->py + particula->down; y++)
		for (x = particula->px - particula->left; x < particula->px + particula->right;	x++)
			if( pfuncionTest(&(imagen->pixel[y][x])) )
				peso++;
	#ifdef ALGORITMOII_H_DEBUG
		if (peso>0)
			printf("PESO=%d\n", peso);
	#endif

	particula->peso = peso;
	return bounded;
}

void growParticle(tParticula *particula, const ImagenRGB *imagen, unsigned int grow_step, unsigned int p_np_relation, bool(*pfuncionTest)(const rgb_color* pixel)){
	int x=0,y=0;
	unsigned int n;
	unsigned int up=particula->up, down=particula->down, left=particula->left, right=particula->right;
	unsigned int peso, peso_ant;
	unsigned int np,npa;//np = peso de las part�culas negras (npa = np anterior)
	unsigned int area;
	int crecimiento;

	n=0;        
	do{
		peso_ant=0; npa=0;
		for(;;){
			darPeso(particula, imagen, pfuncionTest);
			peso=particula->peso; 

			area=((particula->up + particula->down) * (particula->right + particula->left));
			np=area-peso;
			crecimiento = (peso-peso_ant) - (np-npa);
//printf("a:%d c:%d p:%d p':%d n:%d n':%d [%d<%d>%d-%d<%d>%d]\n", area, crecimiento, peso, peso_ant, np,npa, up, py, down, left, px, right);
				
			if (crecimiento == 0){
				break;
			}else if(p_np_relation*(peso-peso_ant)>np-npa){  //crecimiento de part�culas blancas > crecim. de las negras -> el recuadro crece si se toma m�s objeto que fondo					
				switch(n%4){  //estructura para que e recuadro crezca en los 4 sentidos
					case 0: particula->up+=grow_step; 
//							cout<<"up++"<<endl;
					break;
					case 1: particula->down+=grow_step; 
//							cout<<"down++"<<endl; 
					break;
					case 2: particula->left+=grow_step; 
//							cout<<"left++"<<endl; 
					break;
					case 3: particula->right+=grow_step; 
//							cout<<"right++"<<endl; 
					break;
				}
			}
			peso_ant=peso;
			npa=np;
		}//for;
		n++;
	}while(n<8);//para que haga 2 pasadas de cada opci�n (m�nimo=3, 1 pasada).
}




void shrinkParticle(tParticula *particula, const ImagenRGB *imagen, unsigned int shrink_step, unsigned int np_p_relation, bool(*pfuncionTest)(const rgb_color* pixel)){
	int x=0,y=0;
	unsigned int n;
	int px,py;
	unsigned int p, pa; //p = n� de coincidencias positivas (pa = p anterior)
	unsigned int np,npa;//np = n� de coincidencias negativas (npa = np anterior)
	unsigned int area;
	int crecimiento;

	n=0;
	do{
		pa=0; npa=0;
		for(;;){
			darPeso(particula, imagen, pfuncionTest);
			p=particula->peso;

			area=((particula->up + particula->down) * (particula->right + particula->left));
			np=area-p;
			crecimiento = (p-pa) - (np-npa);
			
			if (crecimiento < 0 || area == 0){
				break;
			}else if(np_p_relation*(npa-np) > pa-p){  //decrecimiento de part�culas negativas > decrecim. de las positivas -> el recuadro encoge si se toma m�s objeto que fondo					
				switch(n%4){
					case 0: if (particula->up > shrink_step) particula->up-=shrink_step;
						else particula->up = 0;
						cout<<"up--"<<endl;

					break;
					case 1: if (particula->down > shrink_step) particula->down-=shrink_step;
						else particula->down = 0;
						cout<<"down--"<<endl;

					break;
					case 2: if (particula->left > shrink_step) particula->left-=shrink_step;
						else particula->left = 0;
						cout<<"left--"<<endl;
					break;
					case 3: if (particula->right > shrink_step) particula->right-=shrink_step;
						else particula->right = 0;
						cout<<"right--"<<endl;
					break;
				}

			}
			pa=p;
			npa=np;
		}//for;
		n++;
	}while(n<8);//para que haga 5 pasadas de cada opci�n (m�nimo=3).
}

void recuadrarObjeto(ImagenRGB *imagen, const tParticula *particula, void(*pfuncionPintar)(rgb_color* pixel)){
	unsigned int x,y;

	//uso de variables auxiliares para acortar el c�digo
	unsigned int px    = particula->px;
	unsigned int py    = particula->py;
	unsigned int up    = particula->up;
	unsigned int down  = particula->down;
	unsigned int right = particula->right;
	unsigned int left  = particula->left;

	unsigned int x_max = imagen->WIDTH;
	unsigned int y_max = imagen->HEIGHT;
	//

	x=px-left;
	while(x<px+right && x<x_max && x>=0){
		y=py-up;
		while(y<py+down && y<y_max && y>=0){
			(*pfuncionPintar)(&(imagen->pixel[y][x]));
			if(x==px-left || x==px+right-1){
				y++;
			}else{
				y+=left+right-1;
			}
		}
		x++;
	}
}

}}//namespace

