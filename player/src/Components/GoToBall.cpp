#include "GoToBall.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

GoToBall::GoToBall()
{
	_NewBallDetector = NewBallDetector::getInstance();
	_Head = Head::getInstance();
	_Kinematics = Kinematics::getInstance();
	_Body = Body::getInstance();
	state = Initial;
        
        this->setFreqTime(_Body->getFreqTime()*2);
}

GoToBall::~GoToBall()
{

}

void GoToBall::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//    	setFreqTime(ImageInput::CAPTURE_RATE);
//    	setFreqTime(250);
        
        v_bool=true;        
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void GoToBall::Moving_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving_state_code
	if (!_NewBallDetector->isDetected()) return;
        
        _NewBallDetector->getImagePosition(ball_p2d);
        _NewBallDetector->get2DPosition(ball_p2d);
        _NewBallDetector->get3DPosition(ball_p3d);
//        printf("x2d=%.2f, y2d=%.2f\n", ball_p2d.x, ball_p2d.y);
//        printf("x3d=%.2f, y3d=%.2f, z3d=%.2f\n", ball_p3d.X, ball_p3d.Y, ball_p3d.Z);
        
        pan  = _Head->getPan();  //horizontal
        tilt = _Head->getTilt(); //vertical
        
	_Kinematics->cloneKinematics(&myKinematics);
        pan = myKinematics.pan;
        tilt = myKinematics.tilt;
//        printf("pan=%.2f, tilt=%.2f\n", pan, tilt);
/*LO DE VICTOR
        pan = getValue("HeadYaw");
        tilt = getValue("HeadPitch");
        printf("pan=%.2f, tilt=%.2f\n", pan, tilt);
*/
        
//        v_bool = !v_bool;
//        _Head->setPanPos( (v_bool)?6.2:-6.1 , 0.3);
//        _Head->setTiltPos( (v_bool)?6.2:-6.1 , 0.1);
//        _Head->setPanPos( (pan+TARGET_PAN)/2, 0.2);
//        _Head->setTiltPos( (tilt+TARGET_TILT)/2, 0.2);
        
        float forward, lateral, angular; //Lo que se va a mover

        //FORWARD. using 3D coords
        {
            float limits[] = {BALL_X, BALL_X*1.10, BALL_X*1.20};
            float values[] = {0, 0.2, 0.5};
            forward = getValueAsMin(ball_p3d.X, 1, limits, values, 3);
        }
        
        //LATERAL. 3D coords
        {
            float value = ball_p3d.Y - BALL_Y;
            float _value = fabs(value);
            float limits[] = {_value, _value*1.10, _value*1.20};
            float values[] = {0, 0.1, 0.2};
            lateral = sign(ball_p3d.Y) * getValueAsMin(fabs(ball_p3d.Y), 0.5, limits, values, 3);
        }

        //LATERAL. 2D coords
//        {
//            float limits[] = {10, 20, 50, 100};
//            float values[] = {0, 0.1, 0.2, 0.5};
//            lateral = sign(160-ball_p2d.y) * getValueAsMin(fabs(160-ball_p2d.y), 0.8, limits, values, 4);
//        }

        //ANGULAR. Neck desviation
        {
	    float value = myKinematics.pan;//DAVID
            //float value = getValue("HeadYaw"); //VICTOR
            float limits[] = {0.02, 0.1, 0.2, 0.5};
            float values[] = {0, 0.05, 0.2, 0.3};
            angular = sign(value) * getValueAsMin(fabs(value), 0.8, limits, values, 4);
        }
        
//        forward = 0;
//        lateral = 0;
//	angular = 0;
        
        //forward = sign(ball_p3d.X) * 1.0;
        //lateral = sign(ball_p3d.Y) * 1.0;
        //angular = sign(pan) * 1.0;
        //angular = pan/6.28;
/*
	if (fabs(angular) > 0.1){
		FORWARD = 0.0;
		LATERAL = 0.0;
		ANGULAR = angular;
	}else{
		FORWARD = forward;
		LATERAL = lateral;
		ANGULAR = 0.0;
	}
*/
	FORWARD = forward;
	LATERAL = lateral;
	ANGULAR = angular;

        _Body->setVel(FORWARD, ANGULAR, LATERAL);
//        printf("y=%.03f\n", 160-ball_p2d.y);
//        printf("forward=%.03f, lateral=%.03f, angular=%.03f\n", FORWARD, LATERAL, ANGULAR);
//BUILDER COMMENT. DO NOT REMOVE. end Moving_state_code
}

void GoToBall::Done_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Done_state_code
	_NewBallDetector->get3DPosition(ball_p3d);
//BUILDER COMMENT. DO NOT REMOVE. end Done_state_code
}

void GoToBall::WaitForBall_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin WaitForBall_state_code
//BUILDER COMMENT. DO NOT REMOVE. end WaitForBall_state_code
}

bool GoToBall::Moving2Done_BallIsNear_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving2Done_BallIsNear_transition_code
        bool test_value;
        test_value = (fabs(pan-TARGET_PAN) < pan_error) && (fabs(tilt-TARGET_TILT) < tilt_error);
        test_value = (ball_p3d.X < BALL_X) && (fabs(ball_p3d.Y - BALL_Y) < y_error);
        if (test_value){
            cout<<"GTB Done!";
            _Body->setVel(0,0,0);
            _Head->setPan(0.0);
            _Head->setTilt(0.0);
        }
        return test_value;
//BUILDER COMMENT. DO NOT REMOVE. end Moving2Done_BallIsNear_transition_code
}

bool GoToBall::Done2Moving_BallIsFar_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Done2Moving_BallIsFar_transition_code
        bool test_value;
        //test_value = (fabs(pan-TARGET_PAN) > pan_error) && (fabs(tilt-TARGET_TILT) > tilt_error);
        test_value = (ball_p3d.X > BALL_X);// || (fabs(ball_p3d.Y - BALL_Y) > y_error);
	cout<<"Exit from Done? "<<test_value<<endl;
        return test_value;
//BUILDER COMMENT. DO NOT REMOVE. end Done2Moving_BallIsFar_transition_code
}

bool GoToBall::Initial2WaitForBall0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2WaitForBall0_transition_code
	return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2WaitForBall0_transition_code
}

bool GoToBall::WaitForBall2Moving_BallDetected_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin WaitForBall2Moving_BallDetected_transition_code
	return _NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end WaitForBall2Moving_BallDetected_transition_code
}

bool GoToBall::Moving2WaitForBall_NotDetected_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Moving2WaitForBall_NotDetected_transition_code
	if ( !_NewBallDetector->isDetected() ){
		_Body->setVel(0,0,0);
		_Head->setPan(0.0);
		_Head->setTilt(0.0);

cout<<"GTB ball losed" << endl;

		return true;
	}
	return false;
//BUILDER COMMENT. DO NOT REMOVE. end Moving2WaitForBall_NotDetected_transition_code
}

bool GoToBall::Done2WaitForBall_BallLost_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Done2WaitForBall_BallLost_transition_code
	return !_NewBallDetector->isDetected();
//BUILDER COMMENT. DO NOT REMOVE. end Done2WaitForBall_BallLost_transition_code
}

void
GoToBall::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2WaitForBall0_transition_code()) {
				state = WaitForBall;
			}
		}

		break;
	case Moving:
		_NewBallDetector->step();
		_Head->step();
		_Kinematics->step();

		if (isTime2Run()) {
			Moving_state_code();

			if (Moving2Done_BallIsNear_transition_code()) {
				state = Done;
			}
			else if (Moving2WaitForBall_NotDetected_transition_code()) {
				state = WaitForBall;
			}
		}

		_Body->step();
		break;
	case Done:
		_Head->step();
		_NewBallDetector->step();

		if (isTime2Run()) {
			Done_state_code();

			if (Done2Moving_BallIsFar_transition_code()) {
				state = Moving;
			}
			else if (Done2WaitForBall_BallLost_transition_code()) {
				state = WaitForBall;
			}
		}

		break;
	case WaitForBall:
		_NewBallDetector->step();

		if (isTime2Run()) {
			WaitForBall_state_code();

			if (WaitForBall2Moving_BallDetected_transition_code()) {
				state = Moving;
			}
		}

		_Body->step();
		break;
	default:
		cout << "[GoToBall::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
void
GoToBall::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker) {
	Component::init(newName, parentBroker);
        try{
		pmotion = parentBroker->getMotionProxy();
	}catch( AL::ALError& e) {
		cerr << "[JointControl ()::init(): " << e.toString() << endl;
	}
}


bool 
GoToBall::moveIsDone()
{
	return state == Done;
}

float
GoToBall::getValue(string str)
{
        AL::ALValue args;
        args.arraySetSize(1);
	args[0] = str;
        
        return (pmotion->getAngles(args, false)[0]);
}

float
GoToBall::getValueAsMin(float in_value, float out_default, float limits[], float values[], int N)
{
        for (int i=0; i<N; i++){
            if (in_value < limits[i]){
                return values[i];
            }
        }
        return out_default;
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

