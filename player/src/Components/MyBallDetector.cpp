#include "MyBallDetector.h"


MyBallDetector::MyBallDetector()
{
	_CameraRGB = CameraRGB::getInstance();
	_Kinematics =  Kinematics::getInstance();

	memset(&particula, 0, sizeof(image::filter::tParticula));

	this->setFreqTime(_CameraRGB->getFreqTime());

//	initLog("MBD");
//	writeLog(" -- Inicio de log -- \n");
}

MyBallDetector::~MyBallDetector()
{

}

void
MyBallDetector::step()
{
	_CameraRGB->step();
	_Kinematics->step();


	if (isTime2Run()){
		this->detect();
	}

}


//inline
bool
funcionTest(const image::rgb_color *pixel){
	image::hsv_color hsv;
	image::rgb2hsv(*pixel, hsv);
	if ( 
		(hsv.h > 20 && hsv.h < 80)
		&& (hsv.s > 210 && hsv.s < 256)
//		(hsv.h > 30 && hsv.h < 40)
//		&& (hsv.s > 210 && hsv.s < 240)
		//&& (hsv.v > 160 && hsv.v < 255)
		//&& ( abs(hsv.s/(float)hsv.v -1.34) < 0.1)
	){
printf("hsv: %d %d %d\n", hsv.h, hsv.s, hsv.v);
		return true;
	}else{
		return false;
	}
}


void
MyBallDetector::blackWhiteBall()
{
	image::rgb_color positive = {255,255,255};
	image::rgb_color negative = {0,0,0};
	image::filter::umbralizar(&imagenRGB, funcionTest, positive, negative);
}

void
MyBallDetector::blackNoBall()
{
	image::rgb_color color = {0,0,0};
	for (int h=0; h<imagenRGB.HEIGHT; h++)
		for (int w=0; w<imagenRGB.WIDTH; w++)
			if ( !funcionTest(&imagenRGB.pixel[h][w]) )
				imagenRGB.pixel[h][w]=color;
}


void
MyBallDetector::shapeBall()
{
	int ancho=10, alto=10;

	if (particula.peso > 0){
		ancho = (particula.left+particula.right)/2;
		alto  = (particula.up+particula.down)/2;
	}

	image::filter::searchBest(
		&particula, 
		&imagenRGB, 
		ancho,alto, 
		ancho,alto, 
		funcionTest,
		0,0
	);

	if (particula.peso > 0){
		image::filter::growParticle(&particula, &imagenRGB, 3, 1, funcionTest);
		image::filter::shrinkParticle(&particula, &imagenRGB, 3, 2, funcionTest);
		int cW = particula.px - particula.left + particula.right;
		int cH = particula.py -particula.up + particula.down;	
		printf("Founded! %d [%d][%d]:[%d][%d] {%d,%d,%d,%d}\n", particula.peso, cW,cH, particula.px, particula.py, particula.left, particula.right, particula.up, particula.down);
	}
}




void
MyBallDetector::luckyShapeBall()
{
	int MIN = 5;
	int ancho=5, alto=5;

	if (particula.peso > 0){
		ancho = (particula.left + particula.right) / 4; if(ancho < MIN) ancho = MIN;
		alto  = (particula.up   + particula.down)  / 4; if(alto  < MIN) alto  = MIN;
	}

	image::filter::luckyCenter(
		&particula, 
		&imagenRGB, 
		ancho,alto, 
		funcionTest
	);

	image::rgb_color rgb_net = {0,0,0};
	image::filter::paintNet(&imagenRGB, ancho,alto, rgb_net);

	if (particula.peso > 0){
		printf("Founded! %d [%d][%d]/%d:%d {%d,%d,%d,%d}\n", particula.peso,
			particula.px, particula.py, ancho, alto,
			particula.left, particula.right, particula.up, particula.down
		);
	}
}



void
MyBallDetector::luckyShapeBall2()
{
	int MIN = 3;
	int MAX = 13;
	int ancho=7, alto=7;

	int x_min,y_min, x_max,y_max;
	int k_t = 10;

	if (particula.peso > 0){
		ancho = (particula.left + particula.right) >> 2; if(ancho < MIN) ancho = MIN; else if(ancho > MAX) ancho = MAX;
		alto  = (particula.up   + particula.down)  >> 2; if(alto  < MIN) alto  = MIN; else if(alto  > MAX) alto  = MAX;

		x_min = particula.px - (k_t + particula.left);   if (x_min < 0) x_min = 0;
		y_min = particula.py - (k_t + particula.up + 1); if (y_min < 0) y_min = 0;

		x_max = particula.px + (k_t + particula.right);    if (x_max > imagenRGB.WIDTH ) x_max = imagenRGB.WIDTH;
		y_max = particula.py + (k_t + particula.down + 1); if (y_max > imagenRGB.HEIGHT) y_max = imagenRGB.HEIGHT;

		image::filter::luckyCenter(
			&particula, 
			&imagenRGB, 
			ancho,alto,
			x_min,y_min,
			x_max,y_max,
			funcionTest
		);
	}else{
		x_min = 0;
		y_min = 0;
		x_max = imagenRGB.WIDTH;
		y_max = imagenRGB.HEIGHT;

		image::filter::luckyCenter(
			&particula, 
			&imagenRGB, 
			ancho,alto, 
			funcionTest
		);
	}

	image::rgb_color rgb_net = {0,0,0};
	image::filter::paintNet(
		&imagenRGB,
		ancho,alto,
		x_min,y_min,
		x_max,y_max,
		rgb_net
	);

	if (particula.peso > 0){
		printf("Founded! %d [%d][%d]/%d:%d {%d,%d,%d,%d}\n", particula.peso,
			particula.px, particula.py, ancho, alto,
			particula.left, particula.right, particula.up, particula.down
		);
	}
}



void
MyBallDetector::bestAndluckyShapeBall()
{
	int MIN = 3;
	int MAX = 13;
	int ancho=7, alto=7;

	int x_min,y_min, x_max,y_max;
	int k_t = 10;

	if (particula.peso > 0){
		ancho = (particula.left + particula.right) >> 2; if(ancho < MIN) ancho = MIN; else if(ancho > MAX) ancho = MAX;
		alto  = (particula.up   + particula.down)  >> 2; if(alto  < MIN) alto  = MIN; else if(alto  > MAX) alto  = MAX;

		x_min = particula.px - (k_t + particula.left);   if (x_min < 0) x_min = 0;
		y_min = particula.py - (k_t + particula.up + 1); if (y_min < 0) y_min = 0;

		x_max = particula.px + (k_t + particula.right);    if (x_max > imagenRGB.WIDTH ) x_max = imagenRGB.WIDTH;
		y_max = particula.py + (k_t + particula.down + 1); if (y_max > imagenRGB.HEIGHT) y_max = imagenRGB.HEIGHT;

		image::filter::luckyCenter(
			&particula, 
			&imagenRGB, 
			ancho,alto,
			x_min,y_min,
			x_max,y_max,
			funcionTest
		);
	}else{
		x_min = 0;
		y_min = 0;
		x_max = imagenRGB.WIDTH;
		y_max = imagenRGB.HEIGHT;

		image::filter::searchBest(
			&particula, 
			&imagenRGB, 
			ancho,alto,
			ancho,alto,
			funcionTest
		);
	}

	image::rgb_color rgb_net = {0,0,0};
	image::filter::paintNet(
		&imagenRGB,
		ancho,alto,
		x_min,y_min,
		x_max,y_max,
		rgb_net
	);

	if (particula.peso > 0){
		printf("Founded! %d [%d][%d]/%d:%d {%d,%d,%d,%d}\n", particula.peso,
			particula.px, particula.py, ancho, alto,
			particula.left, particula.right, particula.up, particula.down
		);
	}
}


void
MyBallDetector::formsDebug()
{
	image::forms::square(imagenRGB, 100, 200, 20, 0);	
	image::forms::rhombus(imagenRGB, 200, 200, 20, 0);	
	image::forms::circle(imagenRGB, 100, 100, 50, 0);
}

void 
MyBallDetector::hsvDebug()
{
	for (int h=0; h<imagenRGB.HEIGHT; h++)
		for (int w=0; w<imagenRGB.WIDTH; w++){
			image::hsv_color hsv;
			image::rgb2hsv(imagenRGB.pixel[h][w], hsv);
			printf("hsv: %d %d %d\n", hsv.h, hsv.s, hsv.v);
		}
}

void
MyBallDetector::detect()
{
	startDebugInfo();
	_CameraRGB->getImageCopy((char*)imagenRGB.pixel);
	
//	blackNoBall();
//	blackWhiteBall();
//	shapeBall();
//	luckyShapeBall();
//	luckyShapeBall2();
	bestAndluckyShapeBall();

//	formsDebug();
//	hsvDebug();

	endDebugInfo();

	if (particula.peso > 0){
		HPoint2D p2d;
		HPoint3D p3d;
		_Kinematics->get3DPositionZ(p3d, p2d, particula.down);
		printf("3D x:%.2f y:%.2f z:%.2f\n", p3d.X, p3d.Y, p3d.Z);
	}

}


bica::ShapeList 
MyBallDetector::getGrDebugImg()
{
	shapeListImg.clear();		

	bica::ImagePtr image(new bica::Image);
	image->pixelData.resize(imagenRGB.SIZE);
	image->width = imagenRGB.WIDTH;
	image->height = imagenRGB.HEIGHT;
	image->color = bica::RED;
	image->filled = false;
	image->opacity = 125;
	image->accKey = "b";
	image->label = "";
	memmove( &(image->pixelData[0]), (char *) imagenRGB.pixel, imagenRGB.SIZE);
	shapeListImg.push_back(image);

	if (particula.peso > 0){	
		printf("Marking Ball...\n");

		bica::Point3DPtr p1(new bica::Point3D);
		bica::Point3DPtr p2(new bica::Point3D);
		bica::RectanglePtr r(new bica::Rectangle);
	
		p1->x = particula.px-particula.left;
		p1->y = particula.py-particula.up;
		p1->z = 0;
		p2->x = particula.px+particula.right;
		p2->y = particula.py+particula.down;
		p2->z = 0.0;

		r->p1 = p1;
		r->p2 = p2;
		r->color = bica::PURPLE;
		r->filled = false;
		r->opacity = 125;
		r->accKey = "b";
		r->label = "";

		shapeListImg.push_back(r);
	}

	return shapeListImg;
}
