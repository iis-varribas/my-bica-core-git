#include "BumperDetector.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

BumperDetector::BumperDetector()
{
	state = Initial;
        
       setFreqTime(MEDIUM_RATE);
}

BumperDetector::~BumperDetector()
{

}

void BumperDetector::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void BumperDetector::BumperMemoryRequest_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin BumperMemoryRequest_state_code
float Ll,Lr,Rl,Rr;

try{
	Lr = (float)(pmemory->getData(string("Device/SubDeviceList/LFoot/Bumper/Right/Sensor/Value"), 1));
	Ll = (float)(pmemory->getData(string("Device/SubDeviceList/LFoot/Bumper/Left/Sensor/Value"), 1));

	Rr = (float)(pmemory->getData(string("Device/SubDeviceList/RFoot/Bumper/Right/Sensor/Value"), 1));
	Rl = (float)(pmemory->getData(string("Device/SubDeviceList/RFoot/Bumper/Left/Sensor/Value"), 1));
} catch (ALError& e)
{
	cerr << "BumperDetector::step(ALMemory->getData)" << e.toString() << endl;
}

LFootRBumper = (Lr != 0)? true : false;
LFootLBumper = (Ll != 0)? true : false;

RFootRBumper = (Rr != 0)? true : false;
RFootLBumper = (Rl != 0)? true : false;

//@@ cout<<"BumperDetector::request [Ll="<<Ll<<" Lr="<<Lr<<" Rl="<<Rl<<" Rr="<<Rr<<"]\n";
//BUILDER COMMENT. DO NOT REMOVE. end BumperMemoryRequest_state_code
}

bool BumperDetector::Initial2BumperMemoryRequest0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2BumperMemoryRequest0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2BumperMemoryRequest0_transition_code
}

void
BumperDetector::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2BumperMemoryRequest0_transition_code()) {
				state = BumperMemoryRequest;
			}
		}

		break;
	case BumperMemoryRequest:

		if (isTime2Run()) {
			BumperMemoryRequest_state_code();

		}

		break;
	default:
		cout << "[BumperDetector::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
void
BumperDetector::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker) {
	Component::init(newName, parentBroker);
	
	try
	{
		pmemory = getParentBroker()->getMemoryProxy();
	}catch( AL::ALError& e )
	{
		cerr<<"BumperDetector::init [pmemory]"<<e.toString()<<endl;
	}

}

void
BumperDetector::getValues(bool &LFootL, bool &LFootR, bool &RFootL, bool &RFootR)
{
	LFootR = LFootRBumper;
	LFootL = LFootLBumper;
	RFootL = RFootLBumper;
	RFootR = RFootRBumper;
}


bool 
BumperDetector::isRightFootPressed()
{
	return RFootRBumper && RFootLBumper;
}

bool 
BumperDetector::isLeftFootPressed()
{
	return LFootRBumper && LFootLBumper;
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

