#ifndef LFootLongPress_H
#define LFootLongPress_H

#include "Component.h"
#include "Singleton.h"

#include "TestLongActivityAbstractComponent.h"
#include "BumperDetector.h"


class LFootLongPress : public TestLongActivityAbstractComponent, public Singleton<LFootLongPress>
{
public:
	LFootLongPress();

	static const int SECONDS = 3;

private:
	bool testFunction(void);

	BumperDetector* _BumperDetector;

};

#endif

