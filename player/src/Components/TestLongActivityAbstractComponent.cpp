#include "TestLongActivityAbstractComponent.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

TestLongActivityAbstractComponent::TestLongActivityAbstractComponent()
{
	state = Initial;

	activation_threshold = 1000;
	test_value = false;
	ACTIVED = false;
}

TestLongActivityAbstractComponent::~TestLongActivityAbstractComponent()
{

}

void TestLongActivityAbstractComponent::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
ACTIVED=false;
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void TestLongActivityAbstractComponent::False_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin False_state_code
test_value = testFunction();
//BUILDER COMMENT. DO NOT REMOVE. end False_state_code
}

void TestLongActivityAbstractComponent::True_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin True_state_code
test_value = testFunction();
//BUILDER COMMENT. DO NOT REMOVE. end True_state_code
}

void TestLongActivityAbstractComponent::Active_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Active_state_code
ACTIVED = test_value = testFunction();
//BUILDER COMMENT. DO NOT REMOVE. end Active_state_code
}

bool TestLongActivityAbstractComponent::Initial2False0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2False0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2False0_transition_code
}

bool TestLongActivityAbstractComponent::False2True0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin False2True0_transition_code
if (activation_threshold > 0 && test_value){
	gettimeofday(&tv_ini,NULL);
	return true;
}else{
	return false;
}

//BUILDER COMMENT. DO NOT REMOVE. end False2True0_transition_code
}

bool TestLongActivityAbstractComponent::True2False0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin True2False0_transition_code
return !test_value;

//BUILDER COMMENT. DO NOT REMOVE. end True2False0_transition_code
}

bool TestLongActivityAbstractComponent::True2Active0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin True2Active0_transition_code
if (test_value){
	gettimeofday(&tv_now,NULL);
	unsigned long diff = (tv_now.tv_sec -tv_ini.tv_sec)*1000000 + (tv_now.tv_usec - tv_ini.tv_usec);
	if (diff >= activation_threshold){
		ACTIVED=true;
		return true;
	}else{
		return false;
	}
}
//BUILDER COMMENT. DO NOT REMOVE. end True2Active0_transition_code
}

bool TestLongActivityAbstractComponent::Active2False0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Active2False0_transition_code
return !ACTIVED;
//BUILDER COMMENT. DO NOT REMOVE. end Active2False0_transition_code
}

bool TestLongActivityAbstractComponent::False2Active0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin False2Active0_transition_code
if (activation_threshold == 0 && test_value){
	ACTIVED=true;
	return true;
}else{
	return false;
}
//BUILDER COMMENT. DO NOT REMOVE. end False2Active0_transition_code
}

void
TestLongActivityAbstractComponent::step(void)
{
int old_state = state;
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2False0_transition_code()) {
				state = False;
			}
		}

		break;
	case False:

		if (isTime2Run()) {
			False_state_code();

			if (False2True0_transition_code()) {
				state = True;
			}
			else if (False2Active0_transition_code()) {
				state = Active;
			}
		}

		break;
	case True:

		if (isTime2Run()) {
			True_state_code();

			if (True2False0_transition_code()) {
				state = False;
			}
			else if (True2Active0_transition_code()) {
				state = Active;
			}
		}

		break;
	case Active:

		if (isTime2Run()) {
			Active_state_code();

			if (Active2False0_transition_code()) {
				state = False;
			}
		}

		break;
	default:
		cout << "[TestLongActivityAbstractComponent::step()] Invalid state\n";
	}
if (old_state!=state) cout<<"TLAAC "<<old_state<<" --> "<<state<<"("<<test_value<<"|"<<ACTIVED<<")\n";
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
void
TestLongActivityAbstractComponent::init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker) {
	Component::init(newName, parentBroker);

	setFreqTime(LONG_RATE);
}

void 
TestLongActivityAbstractComponent::setTimeForActive(unsigned long milis){
	activation_threshold = milis;
}

bool 
TestLongActivityAbstractComponent::isActived(){
	return ACTIVED;
}

void 
TestLongActivityAbstractComponent::getValues(bool &data){
	data = ACTIVED;
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

