#ifndef EvitaObstaculos_H
#define EvitaObstaculos_H

#include "Component.h"
#include "USDMean.h"
#include "Body.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "progeo.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class EvitaObstaculos : public Component, public Singleton<EvitaObstaculos>
{
public:

	EvitaObstaculos();
	~EvitaObstaculos();

	void step();
private:

	static const int Initial	= 0;
	static const int Base	= 1;
	static const int ObsIzq	= 2;
	static const int ObsFront	= 3;
	static const int ObsDer	= 4;
	int state;

	USDMean *_USDMean;
	Body *_Body;

	void Initial_state_code(void);
	void Base_state_code(void);
	void ObsIzq_state_code(void);
	void ObsFront_state_code(void);
	void ObsDer_state_code(void);
	bool Initial2Base0_transition_code(void);
	bool Base2ObsIzq0_transition_code(void);
	bool ObsIzq2ObsFront0_transition_code(void);
	bool ObsDer2ObsFront0_transition_code(void);
	bool ObsFront2ObsIzq0_transition_code(void);
	bool Base2ObsDer0_transition_code(void);
	bool ObsFront2ObsDer0_transition_code(void);
	bool ObsIzq2Base0_transition_code(void);
	bool ObsDer2Base0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
private:
	static const float FRONTAL_MAX = 800; // distancia mÃ¡xima (vel=1)
	static const float FRONTAL_MIN = 475; // distancia mÃ­nima (vel=0). Debe ser menor que OBSTACULO
	static const float FRONTAL_VEL = 0.1; // velocidad mÃ­mina

	static const float OBSTACULO = 500; // Distancia a obstaculo

	HPoint3D left;
	HPoint3D right;
	HPoint3D forward;

	float left_m;
	float right_m;
	float forward_m;

	void loadSensorValues();

private:
	int debug_state;

//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

