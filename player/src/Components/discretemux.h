/* 
 * File:   discretemux.h
 * Author: user
 *
 * Created on 24 de noviembre de 2012, 19:41
 */

#ifndef DISCRETEMUX_H
#define	DISCRETEMUX_H

namespace discretemux{

float getMinValue(const float in_value, const float out_default, const float limits[], const float values[], const int size);
float getMaxValue(const float in_value, const float out_default, const float limits[], const float values[], const int size);

}

#endif	/* DISCRETEMUX_H */

