#include "SiguePelota2d.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

SiguePelota2d::SiguePelota2d()
{
	_Head = Head::getInstance();
	_Body = Body::getInstance();
	_NewBallDetector = NewBallDetector::getInstance();
	state = Initial;
}

SiguePelota2d::~SiguePelota2d()
{

}

void SiguePelota2d::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
	tFueraX=0;
	tFueraY=0;
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void SiguePelota2d::MueveCabeza_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin MueveCabeza_state_code
//_Head->stop();
//_Body->setVel(0,0,0);
        _NewBallDetector->getImagePosition(ball_p2d);
	centroX= (ImageInput::IMG_WIDTH)/2;
	centroY= (ImageInput::IMG_HEIGHT)/2;
	difX= (ball_p2d.x -centroX);
	difY= (ball_p2d.y -centroY);
	distX=0;
	distY=0;

	//Componente derivativa:
	derivX= (diffXAnt-difX)/centroX; // Si hay un cambio muy grande acelera.
	derivY= (diffXAnt-difY)/centroY; //Estan normalizados

	//X  
	if ((difX>100) || (difX<-100)){
		if (tFueraX<1){ //Maximo 1.
			tFueraX+=1/30; //Componente integral(PID). Se incrementa en cada step fuera del centro.(30Hz).
		}
		distX= (difX / (centroX))+tFueraX+(derivX*0.2); //La diferencia hasta el centro en X, normalizada + el factor I.
	}else{
		if ((difX>20) || (difX<-20)){
			tFueraX=0;
			distX= difX / (centroX*2);
		}
	} 

	//Y
	if ((difY>100) || (difY<-100)){
		if (tFueraY<1){ //Maximo 1.
			tFueraY+=1/30; //Componente integral(PID). Se incrementa en cada step fuera del centro.(30Hz).
		}
		distY= (difY / (centroY))+(tFueraY)+(derivY*0.2); //La diferencia hasta el centro en X, normalizada + el factor I.
	}else{
		if ((difY>20) || (difY<-20)){
			tFueraY=0;
			distY= difY / (centroY*2);
		}
	}

printf("distX: %f\n",distX);
	
	_Head->setPan(distX);	
	_Head->setTilt(distY);
//BUILDER COMMENT. DO NOT REMOVE. end MueveCabeza_state_code
}

void SiguePelota2d::BuscaPelota_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin BuscaPelota_state_code
//BUILDER COMMENT. DO NOT REMOVE. end BuscaPelota_state_code
}

bool SiguePelota2d::Initial2BuscaPelota0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2BuscaPelota0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2BuscaPelota0_transition_code
}

bool SiguePelota2d::MueveCabeza2BuscaPelota0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin MueveCabeza2BuscaPelota0_transition_code
return true;
//return !(_BallSearch->getEncontrada());
//BUILDER COMMENT. DO NOT REMOVE. end MueveCabeza2BuscaPelota0_transition_code
}

bool SiguePelota2d::BuscaPelota2MueveCabeza0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin BuscaPelota2MueveCabeza0_transition_code
    _NewBallDetector->getImagePosition(ball_p2d);
	return (ball_p2d.x > movement_threshold || ball_p2d.y > movement_threshold);
//return (_BallSearch->getEncontrada());
//BUILDER COMMENT. DO NOT REMOVE. end BuscaPelota2MueveCabeza0_transition_code
}

void
SiguePelota2d::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2BuscaPelota0_transition_code()) {
				state = BuscaPelota;
			}
		}

		break;
	case MueveCabeza:

		if (isTime2Run()) {
			MueveCabeza_state_code();

			if (MueveCabeza2BuscaPelota0_transition_code()) {
				state = BuscaPelota;
			}
		}

		_Head->step();
		_Body->step();
		break;
	case BuscaPelota:
		_NewBallDetector->step();

		if (isTime2Run()) {
			BuscaPelota_state_code();

			if (BuscaPelota2MueveCabeza0_transition_code()) {
				state = MueveCabeza;
			}
		}

		break;
	default:
		cout << "[SiguePelota2d::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

