#ifndef RFootLongPress_H
#define RFootLongPress_H

#include "Component.h"
#include "Singleton.h"

#include "TestLongActivityAbstractComponent.h"
#include "BumperDetector.h"


class RFootLongPress : public TestLongActivityAbstractComponent, public Singleton<RFootLongPress>
{
public:
	RFootLongPress();
        
        static const int SECONDS = 3;
        
private:
	bool testFunction(void);

	BumperDetector* _BumperDetector;

};

#endif

