#include "EvitaObstaculos.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

EvitaObstaculos::EvitaObstaculos()
{
	_USDMean = USDMean::getInstance();
	_Body = Body::getInstance();
	state = Initial;
}

EvitaObstaculos::~EvitaObstaculos()
{

}

void EvitaObstaculos::Initial_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial_state_code
//BUILDER COMMENT. DO NOT REMOVE. end Initial_state_code
}

void EvitaObstaculos::Base_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Base_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [Base]\n";
	cout<<"Modulo ["<<forward_m<<"]\n";
}

loadSensorValues();

float vel_frontal = (forward_m - FRONTAL_MIN) / (FRONTAL_MAX - FRONTAL_MIN);
if (vel_frontal > 0)
	vel_frontal += FRONTAL_VEL;
else
	vel_frontal -= FRONTAL_VEL;
if (vel_frontal >  1) vel_frontal =  1;
if (vel_frontal < -1) vel_frontal = -1;

_Body->setVel(vel_frontal, 0, 0);
//BUILDER COMMENT. DO NOT REMOVE. end Base_state_code
}

void EvitaObstaculos::ObsIzq_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsIzq_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [ObsIzq]\n";
	cout<<"Modulo ["<<left_m<<"]\n";
}

_Body->setVel(0.1, -0.5, -0.1);

loadSensorValues();
//BUILDER COMMENT. DO NOT REMOVE. end ObsIzq_state_code
}

void EvitaObstaculos::ObsFront_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsFront_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [ObsFront]\n";
	cout<<"Modulo ["<<forward_m<<"]\n";
}

_Body->setVel(0, 1, 0);

loadSensorValues();
//BUILDER COMMENT. DO NOT REMOVE. end ObsFront_state_code
}

void EvitaObstaculos::ObsDer_state_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsDer_state_code
if (debug_state!=state){
	debug_state = state;
	cout<<"DebugState [ObsDer]\n";
	cout<<"Modulo ["<<right_m<<"]\n";
}

_Body->setVel(0.1, 0.5, 0.1);

loadSensorValues();
//BUILDER COMMENT. DO NOT REMOVE. end ObsDer_state_code
}

bool EvitaObstaculos::Initial2Base0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Initial2Base0_transition_code
return true;
//BUILDER COMMENT. DO NOT REMOVE. end Initial2Base0_transition_code
}

bool EvitaObstaculos::Base2ObsIzq0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Base2ObsIzq0_transition_code
return left_m < OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end Base2ObsIzq0_transition_code
}

bool EvitaObstaculos::Base2ObsDer0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin Base2ObsDer0_transition_code
return right_m < OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end Base2ObsDer0_transition_code
}

bool EvitaObstaculos::ObsIzq2ObsFront0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsIzq2ObsFront0_transition_code
return right_m < OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsIzq2ObsFront0_transition_code
}

bool EvitaObstaculos::ObsDer2ObsFront0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsDer2ObsFront0_transition_code
return left_m < OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsDer2ObsFront0_transition_code
}

bool EvitaObstaculos::ObsFront2ObsIzq0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsFront2ObsIzq0_transition_code
return right_m > OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsFront2ObsIzq0_transition_code
}

bool EvitaObstaculos::ObsFront2ObsDer0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsFront2ObsDer0_transition_code
return left_m > OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsFront2ObsDer0_transition_code
}

bool EvitaObstaculos::ObsIzq2Base0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsIzq2Base0_transition_code
return left_m > OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsIzq2Base0_transition_code
}

bool EvitaObstaculos::ObsDer2Base0_transition_code(void)
{
//BUILDER COMMENT. DO NOT REMOVE. begin ObsDer2Base0_transition_code
return right_m > OBSTACULO;
//BUILDER COMMENT. DO NOT REMOVE. end ObsDer2Base0_transition_code
}

void
EvitaObstaculos::step(void)
{
	switch (state)
	{
	case Initial:

		if (isTime2Run()) {
			Initial_state_code();

			if (Initial2Base0_transition_code()) {
				state = Base;
			}
		}

		break;
	case Base:
		_USDMean->step();

		if (isTime2Run()) {
			Base_state_code();

			if (Base2ObsIzq0_transition_code()) {
				state = ObsIzq;
			}
			else if (Base2ObsDer0_transition_code()) {
				state = ObsDer;
			}
		}

		_Body->step();
		break;
	case ObsIzq:
		_USDMean->step();

		if (isTime2Run()) {
			ObsIzq_state_code();

			if (ObsIzq2ObsFront0_transition_code()) {
				state = ObsFront;
				resetStopWatch();
			}
			else if (ObsIzq2Base0_transition_code()) {
				state = Base;
			}
		}

		_Body->step();
		break;
	case ObsFront:
		_USDMean->step();

		if (isTime2Run()) {
			ObsFront_state_code();

			if (ObsFront2ObsIzq0_transition_code()) {
				state = ObsIzq;
			}
			else if (ObsFront2ObsDer0_transition_code()) {
				state = ObsDer;
			}
		}

		_Body->step();
		break;
	case ObsDer:
		_USDMean->step();

		if (isTime2Run()) {
			ObsDer_state_code();

			if (ObsDer2ObsFront0_transition_code()) {
				state = ObsFront;
				resetStopWatch();
			}
			else if (ObsDer2Base0_transition_code()) {
				state = Base;
			}
		}

		_Body->step();
		break;
	default:
		cout << "[EvitaObstaculos::step()] Invalid state\n";
	}
}

//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
void
EvitaObstaculos::loadSensorValues(){
	_USDMean->getValues(left, right);

	forward.X = (left.X + right.X) / 2;
	forward.Y =  left.Y + right.Y;
	forward.Z = (left.Z + right.Z) / 2;

	left_m    = sqrt(left.X*left.X         + left.Y*left.Y         + left.Z*left.Z);
	right_m   = sqrt(right.X*right.X       + right.Y*right.Y       + right.Z*right.Z);
	forward_m = sqrt(forward.X*forward.X   + forward.Y*forward.Y   + forward.Z*forward.Z);

#ifdef COMPONENT_DEBUG
	cout<<"FORWARD ["<<forward.X<<","<<forward.Y<<","<<forward.Z<<"]\n";
	cout<<"Modulo  [R:"<<right_m<<", L:"<<left_m<<", F:"<<forward_m<<"]\n";
#endif
}
//BUILDER COMMENT. DO NOT REMOVE. auxcode end

