#ifndef ColorConfigure_H
#define ColorConfigure_H

#include "Component.h"
#include "CameraRGB.h"
#include "BumperDetector.h"
#include "LFootLongPress.h"
#include "NewBallDetector.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "Debug.h" // class : public DebugIImg
#include "ImageInput.h"
#include "image.h"
#include "algoritmoII.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class ColorConfigure : public Component, public Singleton<ColorConfigure>, public DebugIImg
{
public:

	ColorConfigure();
	~ColorConfigure();

	void step();
private:

	static const int Initial	= 0;
	static const int SetColor	= 1;
	static const int Umbralized	= 2;
	static const int Unused	= 3;
	int state;

	CameraRGB *_CameraRGB;
	BumperDetector *_BumperDetector;
	LFootLongPress *_LFootLongPress;
	NewBallDetector *_NewBallDetector;

	void Initial_state_code(void);
	void SetColor_state_code(void);
	void Umbralized_state_code(void);
	void Unused_state_code(void);
	bool Initial2SetColor0_transition_code(void);
	bool SetColor2Umbralized_LFoot_transition_code(void);
	bool Umbralized2SetColor_RFoot_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
	bica::ShapeList getGrDebugImg();

	static const unsigned int c_ancho=20;
	static const unsigned int c_alto=20;
	static const unsigned int W_c = ImageInput::IMG_WIDTH/2;
	static const unsigned int H_c = ImageInput::IMG_HEIGHT/2;
	static const unsigned int W_m = W_c - (c_ancho)/2;
	static const unsigned int W_M = W_c + (c_ancho)/2 +1;
	static const unsigned int H_m = H_c - (c_alto)/2;
	static const unsigned int H_M = H_c + (c_alto)/2 +1;


	static bool funcionTest(const image::rgb_color *pixel);
	static image::hsv_color hsv_min;
	static image::hsv_color hsv_max;

private:
	image::ImagenRGB imagenRGB;
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

