#ifndef BallSearch_H
#define BallSearch_H

#include "Component.h"
#include "NewBallDetector.h"
#include "Kinematics.h"
#include "Body.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "head/head/Head.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class BallSearch : public Component, public Singleton<BallSearch>
{
public:

	BallSearch();
	~BallSearch();

	void step();
private:

	static const int Initial	= 0;
	static const int Search	= 1;
	static const int Moving	= 2;
	static const int Found	= 3;
	static const int LeftScan	= 4;
	static const int RightScanPan	= 5;
	int state;

	NewBallDetector *_NewBallDetector;
	Head *_Head;
	Kinematics *_Kinematics;
	Body *_Body;

	void Initial_state_code(void);
	void Search_state_code(void);
	void Moving_state_code(void);
	void Found_state_code(void);
	void LeftScan_state_code(void);
	void RightScanPan_state_code(void);
	bool Found2Search0_transition_code(void);
	bool RightScanPan2Moving0_transition_code(void);
	bool LeftScan2Found0_transition_code(void);
	bool Search2Found0_transition_code(void);
	bool RightScanPan2Found0_transition_code(void);
	bool Initial2Found0_transition_code(void);
	bool Search2Moving0_transition_code(void);
	bool Moving2Found0_transition_code(void);
	bool LeftScan2Moving0_transition_code(void);
	bool Moving2RightScanPan0_transition_code(void);
	bool Moving2LeftScan0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
        bool encontrada;
        bool getEncontrada();
	//SIGUEPELOTA2d
        static const float movement_threshold = 0.0;
private:
	float myPan;
	float myTilt;
	TKinematics myKinematics;
	float xRead, yRead, thetaRead,lastx,lasty,lastt,movx,movy,movt;
	int change;
	int MyCam;


//SIGUEPELOTA2d
        HPoint2D ball_p2d;
        
	double centroX;
	double difX;
	double distX;
	double centroY;
	double difY;
	double distY;

	double tFueraX;
	double tFueraY;

	double diffXAnt;
	double diffYAnt;
	double derivX;
	double derivY;

//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

