#include "image.h"



#define MIN3(x,y,z)  ((y) <= (z) ? \
                         ((x) <= (y) ? (x) : (y)) \
                     : \
                         ((x) <= (z) ? (x) : (z)))

#define MAX3(x,y,z)  ((y) >= (z) ? \
                         ((x) >= (y) ? (x) : (y)) \
                     : \
                         ((x) >= (z) ? (x) : (z)))

namespace image{

void rgb2hsv(const rgb_color &rgb, hsv_color &hsv){
    unsigned char rgb_min, rgb_max;
    rgb_min = MIN3(rgb.r, rgb.g, rgb.b);
    rgb_max = MAX3(rgb.r, rgb.g, rgb.b);
    hsv.v = rgb_max;
    if (hsv.v == 0) {
        hsv.h = hsv.s = 0;
        return;
    }
    hsv.s = 255*long(rgb_max - rgb_min)/hsv.v;
    if (hsv.s == 0) {
        hsv.h = 0;
        return;
    }
    /* Compute h */
    if (rgb_max == rgb.r) {
        hsv.h = 0 + 43*(rgb.g - rgb.b)/(rgb_max - rgb_min);
    } else if (rgb_max == rgb.g) {
        hsv.h = 85 + 43*(rgb.b - rgb.r)/(rgb_max - rgb_min);
    } else /* rgb_max == rgb.b */ {
        hsv.h = 171 + 43*(rgb.r - rgb.g)/(rgb_max - rgb_min);
    }
}


} //namespace

