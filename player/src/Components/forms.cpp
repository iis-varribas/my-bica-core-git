#include "forms.h"

namespace image{
namespace forms{

bool square(image::ImagenRGB &imagen, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage){
	image::rgb_color color = {0,255,255};

	for (int h=radius, w=0; h>=w; w++){
		imagen.pixel[H+h][W+w] = color;
		imagen.pixel[H+h][W-w] = color;
		imagen.pixel[H-h][W+w] = color;
		imagen.pixel[H-h][W-w] = color;

		// El buble solo llega ahsta la mitad.
		// La otra mitad es simétrica:
		imagen.pixel[H+w][W+h] = color;
		imagen.pixel[H+w][W-h] = color;
		imagen.pixel[H-w][W+h] = color;
		imagen.pixel[H-w][W-h] = color;
	}

	imagen.pixel[H][W].r = 255;
	imagen.pixel[H][W].g = 255;
	imagen.pixel[H][W].b = 255;

	return false;
}

bool rhombus(image::ImagenRGB &imagen, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage){
	int h = radius;
	int w = 0;
	bool isW = true;

	image::rgb_color rgb;
	rgb.r = 255;
	rgb.g = 0;
	rgb.b = 0;

	for (h=radius, w=0; h>=w; isW = !isW){
		imagen.pixel[H+h][W+w] = rgb;
		imagen.pixel[H+h][W-w] = rgb;
		imagen.pixel[H-h][W+w] = rgb;
		imagen.pixel[H-h][W-w] = rgb;

		// El buble solo llega ahsta la mitad.
		// La otra mitad es simétrica:
		imagen.pixel[H+w][W+h] = rgb;
		imagen.pixel[H+w][W-h] = rgb;
		imagen.pixel[H-w][W+h] = rgb;
		imagen.pixel[H-w][W-h] = rgb;

		if (isW) w++;
		else     h--;
	}

	imagen.pixel[H][W].r = 255;
	imagen.pixel[H][W].g = 255;
	imagen.pixel[H][W].b = 255;

	return false;
}

bool circle(image::ImagenRGB &imagen, unsigned int H, unsigned int W, unsigned int radius, float actication_percentage){
	int f = 1 - radius;
	int ddF_x = 1;
	int ddF_y = -2 * radius;
	int x = 0;
	int y = radius;

	image::rgb_color color = {255,0,255};

	imagen.pixel[H][W + radius] = color;
	imagen.pixel[H][W - radius] = color;
	imagen.pixel[H + radius][W] = color;
	imagen.pixel[H - radius][W] = color;

	while(x < y){
		// ddF_x == 2 * x + 1;
		// ddF_y == -2 * y;
		// f == x*x + y*y - radius*radius + 2*x - y + 1;
		if(f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;    
		imagen.pixel[H + x][W + y] = color;
		imagen.pixel[H - x][W + y] = color;
		imagen.pixel[H + x][W - y] = color;
		imagen.pixel[H - x][W - y] = color;
		imagen.pixel[H + y][W + x] = color;
		imagen.pixel[H - y][W + x] = color;
		imagen.pixel[H + y][W - x] = color;
		imagen.pixel[H - y][W - x] = color;
	}

	imagen.pixel[H][W].r = 255;
	imagen.pixel[H][W].g = 255;
	imagen.pixel[H][W].b = 255;

	return false;
}

}} //namespaces
