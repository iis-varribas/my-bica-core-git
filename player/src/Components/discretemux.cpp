#include "discretemux.h"

namespace discretemux{

float getMinValue(const float in_value, const float out_default, const float limits[], const float values[], const int size){
        for (int i=0; i<size; i++){
            if (in_value < limits[i]){
                return values[i];
            }
        }
        return out_default;
}


float getMaxValue(const float in_value, const float out_default, const float limits[], const float values[], const int size){
        for (int i=0; i<size; i++){
            if (in_value > limits[i]){
                return values[i];
            }
        }
        return out_default;
}



}
