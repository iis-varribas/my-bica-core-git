#include "LFootLongPress.h"

LFootLongPress::LFootLongPress() {
	setTimeForActive(SECONDS*1000000);
	_BumperDetector = BumperDetector::getInstance();
        
        this->setFreqTime(_BumperDetector->getFreqTime());
}



bool 
LFootLongPress::testFunction(void){
	bool Ll,Lr,Rl,Rr;

	_BumperDetector->step();
	_BumperDetector->getValues(Ll,Lr,Rl,Rr);

	return Lr;
}

