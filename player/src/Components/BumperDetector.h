#ifndef BumperDetector_H
#define BumperDetector_H

#include "Component.h"
#include "Singleton.h"

//BUILDER COMMENT. DO NOT REMOVE. auxinclude begin
#include "Body.h"
//BUILDER COMMENT. DO NOT REMOVE. auxinclude end

class BumperDetector : public Component, public Singleton<BumperDetector>
{
public:
	BumperDetector();
	~BumperDetector();

	void step();
private:
	static const int Initial	= 0;
	static const int BumperMemoryRequest	= 1;
	int state;

	void Initial_state_code(void);
	void BumperMemoryRequest_state_code(void);
	bool Initial2BumperMemoryRequest0_transition_code(void);
//BUILDER COMMENT. DO NOT REMOVE. auxcode begin
public:
	void init(const string newName, AL::ALPtr<AL::ALBroker> parentBroker);
	void getValues(bool &LFootLBumper, bool &LFootRBumper, bool &RFootLBumper, bool &RFootRBumper);
	bool isRightFootPressed();
	bool isLeftFootPressed();

private:
	AL::ALPtr<AL::ALMemoryProxy> pmemory;

	bool RFootRBumper;
	bool RFootLBumper;
	bool LFootRBumper;
	bool LFootLBumper;
//BUILDER COMMENT. DO NOT REMOVE. auxcode end
};

#endif

