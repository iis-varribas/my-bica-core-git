/* 
 * Name: image.h
 * @Author: Victor Arribas (v.arribas.urjc@gmail.com)
 * Copyright (c) 2012
 *
 */

#ifndef IMAGE_H_
#define IMAGE_H_


#include "ImageInput.h"


namespace image{

typedef
struct rgb_color {
    unsigned char r, g, b;  /* Channel intensities between 0 and 255 */ 
}rgb_color;

typedef
struct hsv_color {
    unsigned char h;        /* Hue degree between 0 and 255 */
    unsigned char s;        /* Saturation between 0 (gray) and 255 */
    unsigned char v;        /* Value between 0 (black) and 255 */
}hsv_color;

typedef
struct ImagenRGB{
	rgb_color pixel[ImageInput::IMG_HEIGHT][ImageInput::IMG_WIDTH];
	const static unsigned int HEIGHT = ImageInput::IMG_HEIGHT;
	const static unsigned int WIDTH = ImageInput::IMG_WIDTH;
	const static unsigned int SIZE = ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * sizeof(image::rgb_color);
}ImagenRGB;
	

typedef
struct ImagenHSV{
	hsv_color pixel[ImageInput::IMG_HEIGHT][ImageInput::IMG_WIDTH];
	const static unsigned int HEIGHT = ImageInput::IMG_HEIGHT;
	const static unsigned int WIDTH = ImageInput::IMG_WIDTH;
	const static unsigned int SIZE = ImageInput::IMG_WIDTH * ImageInput::IMG_HEIGHT * sizeof(image::hsv_color);
}ImagenHSV;





void rgb2hsv(const rgb_color &rgb, hsv_color &hsv);


}

#endif
