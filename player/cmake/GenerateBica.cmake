##------------------------------------------------------------
## Author:		Gonzalo Abella (abellagonzalo@gmail.com)
## Update:		25/01/2012 by Gonzalo Abella
##------------------------------------------------------------

### BICA generation code begin

set(BICA_ICEFILES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/interfaces/slice/bica)
set(BICA_OUTPUT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/interfaces/cpp/bica)
set(SLICE2CPP slice2cpp) 

set (BICA_FILES
	common.ice
	containers.ice
	datetime.ice
	exceptions.ice
	image.ice
	schedulerManager.ice
	componentsI.ice
	motionManagerI.ice
	debug.ice
	logI.ice
)

foreach (file ${BICA_FILES})
	set (bica_file ${file})
	set(args --output-dir=${BICA_OUTPUT_DIR} -I. -I${BICA_ICEFILES_DIR} ${BICA_ICEFILES_DIR}/${bica_file})
	execute_process(COMMAND ${SLICE2CPP} ${args})	
endforeach()

### Bica generation code end


